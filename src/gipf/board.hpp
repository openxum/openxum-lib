/*
 * @file gipf/board.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GIPF_BOARD_HPP
#define _GIPF_BOARD_HPP 1

#include <map>
#include <vector>

#include <gipf/coordinates.hpp>
#include <gipf/exception.hpp>
#include <gipf/intersection.hpp>
#include <gipf/row.hpp>

namespace openxum { namespace gipf {

enum GameType { BASIC, STANDARD, TOURNAMENT };

class Board
{
public:
    typedef std::map < openxum::gipf::Coordinates,
                       Intersection > Intersections;

    enum Phase { PUT_FIRST_PIECE, PUT_PIECE, PUSH_PIECE, CAPTURE_PIECE };

    Board(GameType type = BASIC);

    virtual ~Board()
    { }

    int blackCapturedPieceNumber() const
    { return mBlackCapturedPieceNumber; }

    int blackPieceNumber() const
    { return mBlackPieceNumber; }

    common::Color current_color() const
    { return mCurrentColor; }
    bool exist_intersection(char letter, int number) const;
    common::Color intersection_color(char letter, int number) const
    { return get_intersection(letter, number)->color(); }
    State intersection_state(char letter, int number) const
    { return get_intersection(letter, number)->state(); }
    SeparatedRows get_rows(common::Color color) const;
    const Intersections& intersections() const
    { return mIntersections; }
    bool is_finished() const;
    Phase phase() const
    { return mPhase; }
    void push_piece(const Coordinates& origin, const Coordinates& destination,
                    common::Color color);
    void put_first_piece(const Coordinates& coordinates, common::Color color,
        bool gipf);
    void put_piece(const Coordinates& coordinates, common::Color color);
    void remove_no_row()
    { }
    void remove_rows(const Rows& rows, common::Color color);
    std::string to_string() const;
    GameType game_type() const
    { return mType; }
    common::Color winner_is() const;

    int whiteCapturedPieceNumber() const
    { return mWhiteCapturedPieceNumber; }

    int whitePieceNumber() const
    { return mWhitePieceNumber; }

    // STRATEGY
    CoordinatesList get_possible_first_putting_list() const;
    CoordinatesList get_possible_pushing_list(const Coordinates& origin) const;
    CoordinatesList get_possible_putting_list() const;

    // Constantes
    static const int begin_diagonal_number[];
    static const int end_diagonal_number[];
    static const char begin_diagonal_letter[];
    static const char end_diagonal_letter[];
    static const int begin_number[];
    static const int end_number[];
    static const char begin_letter[];
    static const char end_letter[];

private:
    void buid_row(char letter, int number, common::Color color, bool& start,
                  Rows& rows, Row& row) const;
    void change_color();
    bool check_column(char l) const;
    bool check_diagonal(int i) const;
    bool check_line(int n) const;
    void complete_row(Row& row, common::Color color) const;
    int find_diagonal(const Coordinates& coordinates) const;
    const Intersection* get_intersection(char letter, int number) const;
    Intersection* get_intersection(char letter, int number);
    void move_piece(const Coordinates& origin, const Coordinates& destination);
    void remove_piece(char letter, int number, common::Color color);
    void remove_piece_from_reserve(common::Color color);
    void remove_row(const Row& row, common::Color color);

    Intersections mIntersections;
    GameType mType;
    common::Color mCurrentColor;
    Phase mPhase;
    int mInitialPlacedPieceNumber;
    int mBlackPieceNumber;
    int mWhitePieceNumber;
    int mBlackCapturedPieceNumber;
    int mWhiteCapturedPieceNumber;
};

}} // namespace openxum gipf

#endif
