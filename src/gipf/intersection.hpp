/*
 * @file gipf/intersection.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GIPF_INTERSECTION_HPP
#define _GIPF_INTERSECTION_HPP 1

#include <common/color.hpp>
#include <gipf/coordinates.hpp>

namespace openxum { namespace gipf {

enum State { VACANT, WHITE_PIECE, WHITE_GIPF, BLACK_PIECE, BLACK_GIPF };

class Intersection
{
public:
    Intersection(const openxum::gipf::Coordinates& coordinates) :
        mCoordinates(coordinates), mState(VACANT)
    { }

    virtual ~Intersection()
    { }

    common::Color color() const;

    const openxum::gipf::Coordinates& coordinates() const
    { return mCoordinates; }

    char letter() const
    { return mCoordinates.letter(); }

    int number() const
    { return mCoordinates.number(); }

    virtual bool operator==(const Intersection& intersection) const
    { mCoordinates == intersection.mCoordinates; }

    void put_piece(common::Color color, bool gipf = false);
    void remove_piece(common::Color& color, bool& gipf);
    State state() const;
    std::string to_string() const;

private:
    openxum::gipf::Coordinates mCoordinates;
    State mState;
};

}} // namespace openxum gipf

#endif
