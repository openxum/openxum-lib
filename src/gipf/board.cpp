/*
 * @file gipf/board.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <common/direction.hpp>
#include <gipf/board.hpp>
#include <algorithm>

using namespace openxum::common;

namespace openxum { namespace gipf {

const int Board::begin_diagonal_number[] = { 5, 4, 3, 2, 1, 1, 1, 1, 1 };
const int Board::end_diagonal_number[] = { 9, 9, 9, 9, 9, 8, 7, 6, 5 };

const char Board::begin_diagonal_letter[] = { 'A', 'A', 'A', 'A', 'A', 'B',
                                              'C', 'D', 'E' };
const char Board::end_diagonal_letter[] = { 'E', 'F', 'G', 'H', 'I', 'I',
                                            'I', 'I', 'I' };

const char Board::begin_letter[] = { 'A', 'A', 'A', 'A', 'A', 'B', 'C', 'D',
                                     'E' };
const char Board::end_letter[] = { 'E', 'F', 'G', 'H', 'I', 'I', 'I', 'I',
                                   'I' };

const int Board::begin_number[] = { 1, 1, 1, 1, 1, 2, 3, 4, 5 };
const int Board::end_number[] = { 5, 6, 7, 8, 9, 9, 9, 9, 9 };

Board::Board(GameType type) : mType(type), mPhase(PUT_FIRST_PIECE),
                              mInitialPlacedPieceNumber(0),
                              mBlackPieceNumber(18), mWhitePieceNumber(18),
                              mBlackCapturedPieceNumber(0),
                              mWhiteCapturedPieceNumber(0)
{
    for (char l = 'A'; l < 'J'; ++l) {
        for (int n = begin_number[l - 'A']; n <= end_number[l - 'A']; ++n) {
            openxum::gipf::Coordinates coordinates(l, n);

            mIntersections.insert(
                std::make_pair(coordinates,
                               Intersection(coordinates)));
        }
    }
    mCurrentColor = WHITE;
}

void Board::buid_row(char letter, int number, Color color, bool& start,
                     Rows& rows, Row& row) const
{
    Coordinates coordinates(letter, number);
    Intersections::const_iterator intersection =
        mIntersections.find(coordinates);

    if (not start and intersection->second.state() != VACANT and
        intersection->second.color() == color) {
        start = true;
        row.push_back(coordinates);
    } else if (start and intersection->second.state() != VACANT and
               intersection->second.color() == color) {
        row.push_back(coordinates);
    } else if (start and (intersection->second.state() != VACANT and
                          intersection->second.color() != color) or (
                              intersection->second.state() == VACANT)) {
        if (row.size() >= 4) {
            complete_row(row, color);
            rows.push_back(row);
        }
        start = false;
        row.clear();
    }
}

void Board::change_color()
{
    if (mCurrentColor == WHITE) {
        mCurrentColor = BLACK;
    } else {
        mCurrentColor = WHITE;
    }
}

bool Board::check_column(char l) const
{
    bool found = false;
    int n = begin_number[l - 'A'] + 1;

    while (not found and n < end_number[l - 'A']) {
        if (get_intersection(l, n)->state() == VACANT) {
            found = true;
        } else {
            ++n;
        }
    }
    return found;
}

bool Board::check_diagonal(int i) const
{
    bool found = false;
    char l = begin_diagonal_letter[i - 1] + 1;
    int n = begin_diagonal_number[i - 1] + 1;

    while (not found and
           l < end_diagonal_letter[i - 1] and n < end_diagonal_number[i - 1]) {
        if (get_intersection(l, n)->state() == VACANT) {
            found = true;
        } else {
            ++l;
            ++n;
        }
    }
    return found;
}

bool Board::check_line(int n) const
{
    bool found = false;
    char l = begin_letter[n - 1] + 1;

    while (not found and l < end_letter[n - 1]) {
        if (get_intersection(l, n)->state() == VACANT) {
            found = true;
        } else {
            ++l;
        }
    }
    return found;
}

void Board::complete_row(Row& row, Color color) const
{
    if (row.front().letter() == row.back().letter()) {
        bool found = true;
        char l = row.front().letter();
        int n_front = row.front().number();
        int n_back = row.back().number();

        for (int n = n_front - 1; found and n > begin_number[l - 'A']; --n) {
            if (intersection_state(l, n) != VACANT) {
                row.push_front(Coordinates(l, n));
            } else {
                found = false;
            }
        }
        found = true;
        for (int n = n_back + 1; found and n < end_number[l - 'A']; ++n) {
            if (intersection_state(l, n) != VACANT) {
                row.push_back(Coordinates(l, n));
            } else {
                found = false;
            }
        }
    } else if (row.front().number() == row.back().number()) {
        bool found = true;
        int n = row.front().number();
        char l_front = row.front().letter();
        char l_back = row.back().letter();

        for (char l = l_front - 1; found and l > begin_letter[n - 1]; --l) {
            if (intersection_state(l, n) != VACANT) {
                row.push_front(Coordinates(l, n));
            } else {
                found = false;
            }
        }
        found = true;
        for (char l = l_back + 1; found and l < end_letter[n - 1]; ++l) {
            if (intersection_state(l, n) != VACANT) {
                row.push_back(Coordinates(l, n));
            } else {
                found = false;
            }
        }
    } else {
        int n_front = row.front().number();
        int n_back = row.back().number();
        char l_front = row.front().letter();
        char l_back = row.back().letter();

        {
            char l = l_front - 1;
            int n = n_front - 1;

            while (intersection_state(l, n) != VACANT) {
                row.push_front(Coordinates(l, n));
                --l;
                --n;
            }
        }
        {
            char l = l_back + 1;
            int n = n_back + 1;

            while (intersection_state(l, n) != VACANT) {
                row.push_back(Coordinates(l, n));
                ++l;
                ++n;
            }
        }
    }
}

bool Board::exist_intersection(char letter, int number) const
{
    openxum::gipf::Coordinates coordinates(letter, number);

    if (coordinates.is_valid()) {
        Intersections::const_iterator it = mIntersections.find(coordinates);

        return it != mIntersections.end();
    } else {
        return false;
    }
}

int Board::find_diagonal(const Coordinates& coordinates) const
{
    bool found = false;
    int i = 0;

    while (not found and i < 9) {
        found = coordinates.letter() == begin_diagonal_letter[i] and
            coordinates.number() == begin_diagonal_number[i];
        if (not found) {
            ++i;
        }
    }
    if (not found) {
        i = 0;
        while (not found and i < 9) {
            found = coordinates.letter() == end_diagonal_letter[i] and
                coordinates.number() == end_diagonal_number[i];
            if (not found) {
                ++i;
            }
        }
    }
    if (found) {
        return i + 1;
    } else {
        return -1;
    }
}

const Intersection* Board::get_intersection(char letter, int number) const
{
    openxum::gipf::Coordinates coordinates(letter, number);
    Intersections::const_iterator it = mIntersections.find(coordinates);

    if (it != mIntersections.end()) {
        return &(it->second);
    } else {
        return 0;
    }
}

Intersection* Board::get_intersection(char letter, int number)
{
    openxum::gipf::Coordinates coordinates(letter, number);
    Intersections::iterator it = mIntersections.find(coordinates);

    if (it != mIntersections.end()) {
        return &(it->second);
    } else {
        return 0;
    }
}

CoordinatesList Board::get_possible_pushing_list(
    const Coordinates& origin) const
{
    CoordinatesList list;
    Direction::const_iterator it = Direction::begin();

    while (it != Direction::end()) {
        // column
        if (*it == NORTH or *it == SOUTH) {
            char l = origin.letter();

            if (l >= 'B' and l <= 'H') {
                if (check_column(l)) {
                    if (origin.number() == begin_number[l - 'A']) {
                        if (*it == NORTH) {
                            list.push_back(
                                Coordinates(l, begin_number[l - 'A'] + 1));
                        }
                    } else {
                        if (*it == SOUTH) {
                            list.push_back(
                                Coordinates(l, end_number[l - 'A'] - 1));
                        }
                    }
                }
            }
        }
        // line
        if (*it == SOUTH_EAST or *it == NORTH_WEST) {
            char n = origin.number();

            if (n >= 2 and n <= 8) {
                if (check_line(n)) {
                    if (origin.letter() == begin_letter[n - 1]) {
                        if (*it == SOUTH_EAST) {
                            list.push_back(
                                Coordinates(begin_letter[n - 1] + 1, n));
                        }
                    } else {
                        if (*it == NORTH_WEST) {
                            list.push_back(
                                Coordinates(end_letter[n - 1] - 1, n));
                        }
                    }
                }
            }
        }
        // diagonal
        if (*it == NORTH_EAST or *it == SOUTH_WEST) {
            int i = find_diagonal(origin);

            if (i >= 2 and i <= 8) {
                if (check_diagonal(i)) {
                    if (origin.letter() == begin_diagonal_letter[i - 1] and
                        origin.number() == begin_diagonal_number[i - 1]) {
                        if (*it == NORTH_EAST) {
                            list.push_back(
                                Coordinates(begin_diagonal_letter[i - 1] + 1,
                                            begin_diagonal_number[i - 1] + 1));
                        }
                    } else {
                        if (*it == SOUTH_WEST) {
                            list.push_back(
                                Coordinates(end_diagonal_letter[i - 1] - 1,
                                            end_diagonal_number[i - 1] - 1));
                        }
                    }
                }
            }
        }
        ++it;
    }
    return list;
}

CoordinatesList Board::get_possible_first_putting_list() const
{
    CoordinatesList list;

    if (intersection_state('B', 2) == VACANT) {
        list.push_back(Coordinates('B', 2));
    }
    if (intersection_state('B', 5) == VACANT) {
        list.push_back(Coordinates('B', 5));
    }
    if (intersection_state('E', 2) == VACANT) {
        list.push_back(Coordinates('E', 2));
    }
    if (intersection_state('E', 8) == VACANT) {
        list.push_back(Coordinates('E', 8));
    }
    if (intersection_state('H', 5) == VACANT) {
        list.push_back(Coordinates('H', 5));
    }
    if (intersection_state('H', 8) == VACANT) {
        list.push_back(Coordinates('H', 8));
    }
    return list;
}

CoordinatesList Board::get_possible_putting_list() const
{
    CoordinatesList list;

    // column
    for (char l = 'B'; l <= 'H'; ++l) {
        if (check_column(l)) {
            list.push_back(Coordinates(l, begin_number[l - 'A']));
            list.push_back(Coordinates(l, end_number[l - 'A']));
        }
    }

    // line
    for (int n = 2; n <= 8; ++n) {
        if (check_line(n)) {
            list.push_back(Coordinates(begin_letter[n - 1], n));
            list.push_back(Coordinates(end_letter[n - 1], n));
        }
    }

    // diagonal
    for (int i = 2; i <= 8; ++i) {
        if (check_diagonal(i)) {
            list.push_back(Coordinates(begin_diagonal_letter[i - 1],
                                       begin_diagonal_number[i - 1]));
            list.push_back(Coordinates(end_diagonal_letter[i -1],
                                       end_diagonal_number[i - 1]));
        }
    }
    return list;
}

SeparatedRows Board::get_rows(Color color) const
{
    Rows rows;

    for (int n = 1; n <= 9; ++n) {
        bool start = false;
        Row row;

        for (char l = begin_letter[n - 1]; l <= end_letter[n - 1]; ++l) {
            buid_row(l, n, color, start, rows, row);
        }
        if (row.size() >= 4) {
            rows.push_back(row);
        }
    }

    for (char l = 'A'; l <= 'I'; ++l) {
        bool start = false;
        Row row;

        for (int n = begin_number[l - 'A']; n <= end_number[l - 'A']; ++n) {
            buid_row(l, n, color, start, rows, row);
        }
        if (row.size() >= 4) {
            rows.push_back(row);
        }
    }

    for (int i = 0; i <= 8; ++i) {
        bool start = false;
        Row row;
        int n = begin_diagonal_number[i];
        char l = begin_diagonal_letter[i];

        while (l <= end_diagonal_letter[i] and n <= end_diagonal_number[i]) {
            buid_row(l, n, color, start, rows, row);
            ++l;
            ++n;
        }
        if (row.size() >= 4) {
            rows.push_back(row);
        }
    }

    SeparatedRows srows;

    if (not rows.empty()) {
        Rows list;

        list.push_back(rows.back());
        srows.push_back(list);
        rows.pop_back();
        while (not rows.empty()) {
            Row row = rows.back();
            bool found = false;
            SeparatedRows::iterator it = srows.begin();

            while (it != srows.end() and not found) {
                Rows::const_iterator itr = it->begin();

                while (itr != it->end() and not found) {
                    if (not row.is_separated(*itr)) {
                        it->push_back(row);
                        found = true;
                    } else {
                        ++itr;
                    }
                }
                ++it;
            }
            if (not found) {
                Rows list;

                list.push_back(row);
                srows.push_back(list);
            }
            rows.pop_back();
        }
    }

    return srows;
}

bool Board::is_finished() const
{
    return mBlackPieceNumber == 0 or mWhitePieceNumber == 0;
}

void Board::move_piece(const Coordinates& origin,
                       const Coordinates& destination)
{
    Color color;
    bool gipf;
    Intersection* to = get_intersection(origin.letter(),
                                        origin.number());
    Intersection* from = get_intersection(destination.letter(),
                                          destination.number());

    from->remove_piece(color, gipf);
    to->put_piece(color, gipf);
}

void Board::push_piece(const Coordinates& origin,
                       const Coordinates& destination,
                       Color color)
{
    Intersections::iterator it_origin = mIntersections.find(origin);
    Intersections::iterator it_destination = mIntersections.find(destination);

    if (it_destination->second.state() == VACANT) {
        Color color;
        bool gipf;

        it_origin->second.remove_piece(color, gipf);
        it_destination->second.put_piece(color, gipf);
    } else {
        int delta_letter = destination.letter() - origin.letter();
        int delta_number = destination.number() - origin.number();
        bool found = false;
        Coordinates coordinates = destination;

        coordinates.move(delta_letter, delta_number);
        while (not found) {
            Intersections::iterator it = mIntersections.find(coordinates);

            found = it->second.state() == VACANT;
            if (not found) {
                coordinates = coordinates.move(delta_letter, delta_number);
            }
        }
        delta_letter = -delta_letter;
        delta_number = -delta_number;
        while (coordinates != origin) {
            move_piece(coordinates,
                       coordinates.move(delta_letter, delta_number));
            coordinates = coordinates.move(delta_letter, delta_number);
        }
    }
    mPhase = PUT_PIECE;
    change_color();
}

void Board::put_first_piece(const Coordinates& coordinates, Color color,
                            bool gipf)
{
    char letter = coordinates.letter();
    int number = coordinates.number();

    if ((letter == 'B' and number == 2) or
        (letter == 'B' and number == 5) or
        (letter == 'E' and number == 2) or
        (letter == 'E' and number == 8) or
        (letter == 'H' and number == 5) or
        (letter == 'H' and number == 8)) {
        Intersections::iterator it = mIntersections.find(coordinates);

        if (it != mIntersections.end()) {
            if (it->second.state() == VACANT) {
                it->second.put_piece(color, gipf);
                ++mInitialPlacedPieceNumber;
                remove_piece_from_reserve(color);
            } else {
                throw new InvalidPutting("gipf: invalid putting");
            }
        }
    } else {
        throw new InvalidPutting("gipf: invalid putting");
    }
    if (mInitialPlacedPieceNumber == 6) {
        mPhase = PUT_PIECE;
    }
    change_color();
}

void Board::put_piece(const Coordinates& coordinates, Color color)
{
    char letter = coordinates.letter();
    int number = coordinates.number();

    if (letter == 'A'  or letter == 'I' or
        number == 1 or number == 9 or
        (letter == 'B' and number == 6) or
        (letter == 'C' and number == 7) or
        (letter == 'D' and number == 8) or
        (letter == 'F' and number == 2) or
        (letter == 'G' and number == 3) or
        (letter == 'H' and number == 4)) {
        Intersections::iterator it = mIntersections.find(coordinates);

        if (it != mIntersections.end()) {
            if (it->second.state() == VACANT) {
                it->second.put_piece(color);
                remove_piece_from_reserve(color);
            } else {
                throw new InvalidPutting("gipf: invalid putting");
            }
        }
    } else {
        throw new InvalidPutting("gipf: invalid putting");
    }
    mPhase = PUSH_PIECE;
}

const char letter_matrix[] = {
    'X', 'X', 'X', 'X', 'Z', 'X', 'X', 'X', 'X', 'X',
    'X', 'X', 'X', 'Z', 'X', 'E', 'X', 'X', 'X', 'X',
    'X', 'X', 'Z', 'X', 'D', 'X', 'F', 'X', 'X', 'X',
    'X', 'Z', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'X',
    'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X',
    'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I',
    'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X',
    'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I',
    'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X',
    'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I',
    'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X',
    'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I',
    'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X',
    'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I',
    'X', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X',
    'X', 'X', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'X',
    'X', 'X', 'X', 'X', 'D', 'X', 'F', 'X', 'X', 'X',
    'X', 'X', 'X', 'X', 'X', 'E', 'X', 'X', 'X', 'X'
};

const char number_matrix[] = {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 9, 0, 0, 0, 0,
    0, 0, 0, 0, 8, 0, 9, 0, 0, 0,
    0, 0, 0, 7, 0, 8, 0, 9, 0, 0,
    0, 0, 6, 0, 7, 0, 8, 0, 9, 0,
    0, 5, 0, 6, 0, 7, 0, 8, 0, 9,
    0, 0, 5, 0, 6, 0, 7, 0, 8, 0,
    0, 4, 0, 5, 0, 6, 0, 7, 0, 8,
    0, 0, 4, 0, 5, 0, 6, 0, 7, 0,
    0, 3, 0, 4, 0, 5, 0, 6, 0, 7,
    0, 0, 3, 0, 4, 0, 5, 0, 6, 0,
    0, 2, 0, 3, 0, 4, 0, 5, 0, 6,
    0, 0, 2, 0, 3, 0, 4, 0, 5, 0,
    0, 1, 0, 2, 0, 3, 0, 4, 0, 5,
    0, 0, 1, 0, 2, 0, 3, 0, 4, 0,
    0, 0, 0, 1, 0, 2, 0, 3, 0, 0,
    0, 0, 0, 0, 1, 0, 2, 0, 0, 0,
    0, 0, 0, 0, 0, 1, 0, 0, 0, 0
};

void Board::remove_piece(char letter, int number, Color color)
{
    Coordinates coordinates(letter, number);
    Intersections::iterator it = mIntersections.find(coordinates);

    if (it != mIntersections.end()) {
        Color removed_color;
        bool gipf;

        it->second.remove_piece(removed_color, gipf);
        if (color == removed_color) {
            if (color == BLACK) {
                ++mBlackPieceNumber;
            } else {
                ++mWhitePieceNumber;
            }
        } else {
            if (removed_color == BLACK) {
                ++mBlackCapturedPieceNumber;
            } else {
                ++mWhiteCapturedPieceNumber;
            }
        }
    } else {
        throw InvalidRemovingRow("gipf: invalid coordinates");
    }
}

void Board::remove_piece_from_reserve(Color color)
{
    if (color == BLACK) {
        --mBlackPieceNumber;
    } else {
        --mWhitePieceNumber;
    }
}

void Board::remove_row(const Row& row, Color color)
{
    Row::const_iterator itr = row.begin();

    while (itr != row.end()) {
        remove_piece(itr->letter(), itr->number(), color);
        ++itr;
    }
}

void Board::remove_rows(const Rows& rows, Color color)
{
    Rows::const_iterator it = rows.begin();

    while (it != rows.end()) {
        remove_row(*it, color);
        ++it;
    }
}

std::string Board::to_string() const
{
    std::string str;
    unsigned int k = 9;

    for (int j = 0; j < 18; ++j) {
        for (int i = 0; i < 10; ++i) {
            char l = letter_matrix[i + 10 * j];
            int n = number_matrix[i + 10 * j];

            if (l != 'X' and l != 'Z') {
                openxum::gipf::Coordinates coordinates(l, n);
                Intersections::const_iterator it =
                    mIntersections.find(coordinates);

                if (it != mIntersections.end()) {
                    str += it->second.to_string();
                }
            } else if (l == 'Z') {
                std::string s("   ");

                s += '0' + k;
                str += s;
                --k;
            } else {
                str += "    ";
            }
        }
        str += "\n";
    }
    str += "     A   B   C   D   E   F   G   H   I\n";
    return str;
}

Color Board::winner_is() const
{
    if (is_finished()) {
        if (mBlackPieceNumber == 0) {
            return WHITE;
        } else {
            return BLACK;
        }
    } else {
        throw InvalidWinner("gipf: game is not finished");
    }
}

}} // namespace openxum gipf
