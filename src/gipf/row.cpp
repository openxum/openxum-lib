/*
 * @file gipf/row.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gipf/row.hpp>
#include <algorithm>
#include <sstream>
#include <string.h>

namespace openxum { namespace gipf {

Row::Row(const Coordinates& begin, const Coordinates& end)
{
    build(begin, end);
}

bool Row::belong(const Coordinates& coordinates) const
{
    return std::find(begin(), end(), coordinates) != end();
}

void Row::decode(const char* msg)
{
    Coordinates begin;
    Coordinates end;

    begin.decode(msg);
    end.decode(msg + 3);
    clear();
    build(begin, end);
}

void Row::encode(char* msg) const
{
    char begin[4];
    char end[4];

    front().encode(begin);
    back().encode(end);
    strcpy(msg, begin);
    strcat(msg, end);
}

bool Row::is_separated(const Row& row) const
{
    bool found = false;
    Row::const_iterator it = row.begin();

    while (not found) {
        found = std::find(begin(), end(), *it) == end();
        ++it;
    }
    return not found;
}

std::string Row::to_string() const
{
    std::stringstream str;
    Row::const_iterator it = begin();

    str << "{ ";
    while (it != end()) {
        str << it->letter() << it->number() << " ";
        ++it;
    }
    str << "}";
    return str.str();
}

void Row::build(const Coordinates& begin, const Coordinates& end)
{
    if (begin.letter() == end.letter()) {
        if (begin.number() < end.number()) {
            int n = begin.number();

            while (n <= end.number()) {
                push_back(Coordinates(begin.letter(), n));
                ++n;
            }
        } else {
            int n = begin.number();

            while (n >= end.number()) {
                push_back(Coordinates(begin.letter(), n));
                --n;
            }
        }
    } else if (begin.number() == end.number()) {
        if (begin.letter() < end.letter()) {
            char l = begin.letter();

            while (l <= end.letter()) {
                push_back(Coordinates(l, begin.number()));
                ++l;
            }
        } else {
            char l = begin.letter();

            while (l >= end.letter()) {
                push_back(Coordinates(l, begin.number()));
                --l;
            }
        }
    } else if (begin.letter() < end.letter()) {
        int n = begin.number();
        char l = begin.letter();

        while (l <= end.letter()) {
            push_back(Coordinates(l, n));
            ++l;
            ++n;
        }
    } else {
        int n = begin.number();
        char l = begin.letter();

        while (l >= end.letter()) {
            push_back(Coordinates(l, n));
            --l;
            --n;
        }
    }
}

}} // namespace openxum gipf
