/*
 * @file gipf/intersection.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gipf/exception.hpp>
#include <gipf/intersection.hpp>
#include <sstream>

#include <iostream>

using namespace openxum::common;

namespace openxum { namespace gipf {

common::Color Intersection::color() const
{
    if (mState == VACANT) {
        throw InvalidColor("gipf: no color");
    } else {
        if (mState == WHITE_GIPF or mState == WHITE_PIECE) {
            return WHITE;
        } else {
            return BLACK;
        }
    }
}

void Intersection::put_piece(common::Color color, bool gipf)
{
    if (gipf) {
        mState = color == WHITE ? WHITE_GIPF : BLACK_GIPF;
    } else {
        mState = color == WHITE ? WHITE_PIECE : BLACK_PIECE;
    }
}

void Intersection::remove_piece(common::Color& color, bool& gipf)
{
    color = (mState == WHITE_GIPF or mState == WHITE_PIECE) ? WHITE : BLACK;
    gipf = (mState == WHITE_GIPF or mState == BLACK_GIPF);
    mState = VACANT;
}

State Intersection::state() const
{
    return mState;
}

std::string Intersection::to_string() const
{
    std::ostringstream str;

    str << "[";
    if (mState == VACANT) {
        str << "  ";
    } else {
        if (color() == BLACK) {
            str << "B";
        } else {
            str << "W";
        }

        if (mState == WHITE_GIPF or mState == BLACK_GIPF) {
            str << "*";
        } else {
            str << " ";
        }
    }
    str << "]";
    return str.str();
}

}} // namespace openxum gipf
