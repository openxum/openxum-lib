INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/src ${OPENXUM_COMMON_BINARY_DIR}/src)

SET(GIPF_HPP coordinates.hpp exception.hpp intersection.hpp board.hpp row.hpp)
SET(GIPF_CPP coordinates.cpp intersection.cpp board.cpp row.cpp)

ADD_LIBRARY(openxum-gipf-common SHARED ${GIPF_HPP};${GIPF_CPP})

INSTALL(TARGETS openxum-gipf-common
  RUNTIME DESTINATION bin
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib)

INSTALL(FILES ${GIPF_HPP} DESTINATION ${OPENXUM_INCLUDE_DIR}/gipf)