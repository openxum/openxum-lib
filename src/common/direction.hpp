/*
 * @file common/direction.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _COMMON_DIRECTION_HPP
#define _COMMON_DIRECTION_HPP 1

namespace openxum { namespace common {

enum direction_t { NORTH_WEST, NORTH, NORTH_EAST, SOUTH_EAST, SOUTH,
                   SOUTH_WEST };

class Direction
{
public:
    class const_iterator
    {
    public:
        const_iterator(const const_iterator& it)
        {
            value = it.value;
            all = it.all;
        }

        const_iterator(direction_t direction = NORTH_WEST) :
            value(direction), all(false)
        { }

        virtual bool operator!=(const const_iterator& it) const
        { return not(value == NORTH_WEST and all); }

        virtual direction_t operator*() const
        { return value; }

        virtual const_iterator& operator++()
        {
            if (value == NORTH_WEST) {
                value = NORTH;
            } else if (value == NORTH) {
                value = NORTH_EAST;
            } else if (value == NORTH_EAST) {
                value = SOUTH_EAST;
            } else if (value == SOUTH_EAST) {
                value = SOUTH;
            } else if (value == SOUTH) {
                value = SOUTH_WEST;
            } else if (value == SOUTH_WEST) {
                all = true;
                value = NORTH_WEST;
            }
            return *this;
        }

    private:
        direction_t value;
        bool all;
    };

    static const_iterator begin()
    { return const_iterator(NORTH_WEST); }

    static const_iterator end()
    { return const_iterator(SOUTH_WEST); }
};

}} // namespace openxum common

#endif
