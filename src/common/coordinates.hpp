/*
 * @file common/coordinates.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _COMMON_COORDINATES_HPP
#define _COMMON_COORDINATES_HPP 1

#include <cmath>
#include <vector>

namespace openxum { namespace common {

class Coordinates
{
public:
    Coordinates(char letter = 'A', int number = 1) :
        mLetter(letter), mNumber(number)
    { }

    virtual ~Coordinates()
    { }

    virtual void decode(const char* msg) =0;

    int distance(const Coordinates& coordinates) const
    {
        if (coordinates.letter() == mLetter) {
            return std::abs((float)(coordinates.number() - mNumber));
        } else {
            return std::abs((float)(coordinates.letter() - mLetter));
        }
    }

    virtual void encode(char* msg) const =0;

    virtual int key() const =0;

    virtual bool is_valid() const =0;

    char letter() const
    { return mLetter; }

    int number() const
    { return mNumber; }

    virtual bool operator==(const Coordinates& coordinates) const
    { return key() == coordinates.key(); }

    virtual bool operator!=(const Coordinates& coordinates) const
    { return key() != coordinates.key(); }

    virtual bool operator<(const Coordinates& coordinates) const
    { return key() < coordinates.key(); }

protected:
    char mLetter;
    int mNumber;
};

}} // namespace openxum common

#endif
