/*
 * @file dvonn/board.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DVONN_BOARD_HPP
#define _DVONN_BOARD_HPP 1

#include <map>
#include <vector>

#include <dvonn/coordinates.hpp>
#include <dvonn/exception.hpp>
#include <dvonn/intersection.hpp>

namespace openxum { namespace dvonn {

class Board
{
public:
    typedef std::map < openxum::dvonn::Coordinates,
                       Intersection > Intersections;

    enum Phase { PUT_DVONN_PIECE, PUT_PIECE, MOVE_STACK };

    Board();

    virtual ~Board()
    { }

    bool can_move(const Coordinates& coordinates) const;
    common::Color current_color() const
    { return mCurrentColor; }
    bool exist_intersection(char letter, int number) const;
    Color intersection_color(char letter, int number) const
    { return get_intersection(letter, number)->color(); }
    State intersection_state(char letter, int number) const
    { return get_intersection(letter, number)->state(); }
    const Intersections& intersections() const
    { return mIntersections; }
    bool is_finished() const;
    void move_stack(const Coordinates& origin,
                    const Coordinates& destination);
    void move_no_stack();
    Phase phase() const
    { return mPhase; }
    void put_dvonn_piece(const Coordinates& coordinates);
    void put_piece(const Coordinates& coordinates, Color color);
    CoordinatesList remove_isolated_stacks();
    void remove_stacks(const CoordinatesList& list);
    std::string to_string() const;
    bool verify_moving(const Coordinates& origin,
                       const Coordinates& destination) const;
    common::Color winner_is() const;

    // STRATEGY
    CoordinatesList get_free_intersections() const;
    CoordinatesList get_possible_moving_stacks(Color color) const;
    CoordinatesList get_stack_possible_move(const Coordinates& origin) const;

    // Constantes
    static const char begin_letter[];
    static const char end_letter[];
    static const int begin_number[];
    static const int end_number[];
    static const char begin_diagonal_letter[];
    static const char end_diagonal_letter[];
    static const int begin_diagonal_number[];
    static const int end_diagonal_number[];

private:
    void change_color();
    const Intersection* get_intersection(char letter, int number) const;
    int get_max_stack_size(Color color) const;
    bool is_connected(const Coordinates& coordinates) const;
    Direction next_direction(Direction direction) const;

    Intersections mIntersections;
    common::Color mCurrentColor;
    Phase mPhase;
    int mPlacedDvonnPieceNumber;
    int mPlacedPieceNumber;
};

}} // namespace openxum dvonn

#endif
