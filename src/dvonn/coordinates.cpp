/*
 * @file dvonn/intersection.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <dvonn/exception.hpp>
#include <dvonn/coordinates.hpp>

using namespace openxum::common;

namespace openxum { namespace dvonn {

Coordinates Coordinates::move(int distance, Direction direction) const
{
    switch (direction) {
    case NORTH_WEST: return Coordinates(mLetter - distance, mNumber - distance);
    case NORTH_EAST: return Coordinates(mLetter, mNumber - distance);
    case EAST: return Coordinates(mLetter + distance, mNumber);
    case SOUTH_EAST: return Coordinates(mLetter + distance, mNumber + distance);
    case SOUTH_WEST: return Coordinates(mLetter, mNumber + distance);
    case WEST: return Coordinates(mLetter - distance, mNumber);
    }
}

}} // namespace openxum dvonn
