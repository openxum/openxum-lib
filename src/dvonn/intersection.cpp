/*
 * @file dvonn/intersection.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <dvonn/exception.hpp>
#include <dvonn/intersection.hpp>
#include <sstream>

using namespace openxum::common;

namespace openxum { namespace dvonn {

Color Intersection::color() const
{
    if (mState == VACANT) {
        throw InvalidColor("dvonn: no color");
    }

    return mStack.color();
}

void Intersection::move_stack_to(Intersection& destination)
{
    Stack stack;

    while (not mStack.empty()) {
        stack.put(mStack.remove_top());
    }
    mState = VACANT;
    while (not stack.empty()) {
        destination.put_piece(stack.remove_top().color());
    }
}

State Intersection::state() const
{
    return mState;
}

std::string Intersection::to_string() const
{
    std::ostringstream str;

    str << "[";
    if (mState == VACANT) {
        str << "    ";
    } else {
        if (mStack.color() == BLACK) {
            str << "B";
        } else if (mStack.color() == WHITE) {
            str << "W";
        } else {
            str << "R";
        }
        if (mStack.size() > 1) {
            str << mStack.size();
            if (mStack.size() < 10) {
                str << " ";
            }
            if (mStack.color() != RED and mStack.dvonn()) {
                str << "R";
            } else {
                str << " ";
            }
        } else {
            str << "   ";
        }
    }
    str << "]";
    return str.str();
}

}} // namespace openxum dvonn
