/*
 * @file dvonn/board.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <algorithm>
#include <dvonn/board.hpp>

using namespace openxum::common;

namespace openxum { namespace dvonn {

const char Board::begin_letter[] = { 'A', 'A', 'A', 'B', 'C' };
const char Board::end_letter[] = { 'I', 'J', 'K', 'K', 'K' };
const int Board::begin_number[] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 3 };
const int Board::end_number[] = { 3, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5 };
const char Board::begin_diagonal_letter[] = { 'A', 'A', 'A', 'B', 'C', 'D',
                                              'E', 'F', 'G', 'H', 'I' };
const char Board::end_diagonal_letter[] = { 'C', 'D', 'E', 'F', 'G', 'H', 'I',
                                            'J', 'K', 'K', 'K' };
const int Board::begin_diagonal_number[] = { 3, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
const int Board::end_diagonal_number[] = { 5, 5, 5, 5, 5, 5, 5, 5, 5, 4, 3 };

Board::Board()
{
    for (char l = 'A'; l < 'L'; ++l) {
        for (int n = begin_number[l - 'A']; n <= end_number[l - 'A']; ++n) {
            openxum::dvonn::Coordinates coordinates(l, n);

            mIntersections.insert(
                std::make_pair(coordinates, Intersection(coordinates,
                                                         VACANT)));
        }
    }
    mCurrentColor = WHITE;
    mPlacedDvonnPieceNumber = 0;
    mPlacedPieceNumber = 0;
    mPhase = PUT_DVONN_PIECE;
}

bool Board::can_move(const Coordinates& coordinates) const
{
    Intersections::const_iterator it = mIntersections.find(coordinates);

    if (it->second.state() == VACANT) {
        throw InvalidIntersection("dvonn: no stack on intersection");
    } else if (it->second.size() == 1 and it->second.dvonn()) {
        throw InvalidIntersection("dvonn: cannot move dvonn piece");
    }

    Direction direction = NORTH_WEST;
    bool stop = false;
    bool found = false;

    while (not stop and not found) {
        Coordinates neighboord = coordinates.move(1, direction);

        if (neighboord.is_valid()) {
            Intersections::const_iterator it = mIntersections.find(neighboord);

            found = it->second.state() == VACANT;
        } else {
            found = true;
        }
        if (direction == WEST) {
            stop = true;
        } else {
            direction = next_direction(direction);
        }
    }
    return found;
}

void Board::change_color()
{
    if (mCurrentColor == WHITE) {
        mCurrentColor = BLACK;
    } else {
        mCurrentColor = WHITE;
    }
}

bool Board::exist_intersection(char letter, int number) const
{
    openxum::dvonn::Coordinates coordinates(letter, number);

    if (coordinates.is_valid()) {
        Intersections::const_iterator it = mIntersections.find(coordinates);

        return it != mIntersections.end();
    } else {
        return false;
    }
}

CoordinatesList Board::get_free_intersections() const
{
    CoordinatesList list;
    Intersections::const_iterator it = mIntersections.begin();

    while (it != mIntersections.end()) {
        if (it->second.state() == VACANT) {
            list.push_back(it->first);
        }
        ++it;
    }
    return list;
}

const Intersection* Board::get_intersection(char letter, int number) const
{
    openxum::dvonn::Coordinates coordinates(letter, number);
    Intersections::const_iterator it = mIntersections.find(coordinates);

    if (it != mIntersections.end()) {
        return &(it->second);
    } else {
        return 0;
    }
}

int Board::get_max_stack_size(Color color) const
{
    int max = 0;
    Intersections::const_iterator it = mIntersections.begin();

    while (it != mIntersections.end()) {
        if (it->second.state() == NO_VACANT and it->second.color() == color) {
            if (it->second.size() > max) {
                max = it->second.size();
            }
        }
        ++it;
    }
    return max;
}

CoordinatesList Board::get_possible_moving_stacks(Color color) const
{
    CoordinatesList list;
    Intersections::const_iterator it = mIntersections.begin();

    while (it != mIntersections.end()) {
        if (it->second.state() == NO_VACANT and it->second.color() == color) {
            if (not(it->second.size() == 1 and it->second.dvonn())) {

                if (can_move(it->second.coordinates()) and
                    not get_stack_possible_move(
                        it->second.coordinates()).empty()) {
                    list.push_back(it->first);
                }
            }
        }
        ++it;
    }
    return list;
}

CoordinatesList Board::get_stack_possible_move(const Coordinates& origin) const
{
    if (can_move(origin)) {
        CoordinatesList list;
        Intersections::const_iterator it = mIntersections.find(origin);
        int size = it->second.size();
        Direction direction = NORTH_WEST;
        bool stop = false;

        while (not stop) {
            Coordinates destination = origin.move(size, direction);
            Intersections::const_iterator destination_it =
                mIntersections.find(destination);

            if (destination.is_valid() and
                destination_it->second.state() == NO_VACANT) {
                list.push_back(destination);
            }
            if (direction == WEST) {
                stop = true;
            } else {
                direction = next_direction(direction);
            }
        }
        return list;
    } else {
        throw InvalidMoving("dvonn: stack cannot move");
    }
}

bool Board::is_finished() const
{
    return mPhase == MOVE_STACK and
        get_possible_moving_stacks(WHITE).empty() and
        get_possible_moving_stacks(BLACK).empty();
}

void Board::move_no_stack()
{
    change_color();
}

void Board::move_stack(const Coordinates& origin,
                       const Coordinates& destination)
{
    Intersections::iterator origin_it = mIntersections.find(origin);

    if (origin_it->second.color() != current_color()) {
        throw InvalidColor("dvonn: invalid color");
    }

    Intersections::iterator destination_it = mIntersections.find(destination);

    origin_it->second.move_stack_to(destination_it->second);
    change_color();
}

Direction Board::next_direction(Direction direction) const
{
    if (direction == NORTH_WEST) {
        return NORTH_EAST;
    } else if (direction == NORTH_EAST) {
        return EAST;
    } else if (direction == EAST) {
        return SOUTH_EAST;
    } else if (direction == SOUTH_EAST) {
        return SOUTH_WEST;
    } else if (direction == SOUTH_WEST) {
        return WEST;
    } else if (direction == WEST) {
        return NORTH_WEST;
    }
}

void Board::put_dvonn_piece(const Coordinates& coordinates)
{
    if (mPlacedDvonnPieceNumber == 3) {
        throw new InvalidPutting("dvonn: invalid putting");
    }

    Intersections::iterator it = mIntersections.find(coordinates);

    if (it != mIntersections.end()) {
        if (it->second.state() == VACANT) {
            it->second.put_piece(RED);
            mPlacedDvonnPieceNumber++;
            if (mPlacedDvonnPieceNumber == 3) {
                mPhase = PUT_PIECE;
            }
        } else {
            throw new InvalidPutting("dvonn: invalid putting");
        }
    }
    change_color();
}

void Board::put_piece(const Coordinates& coordinates, Color color)
{
    if (current_color() != color) {
        throw InvalidColor("dvonn: invalid color");
    }

    Intersections::iterator it = mIntersections.find(coordinates);

    if (it != mIntersections.end()) {
        if (it->second.state() == VACANT) {
            it->second.put_piece(color);
            mPlacedPieceNumber++;
            if (mPlacedPieceNumber == 46) {
                mPhase = MOVE_STACK;
            } else {
                change_color();
            }
        } else {
            throw new InvalidPutting("dvonn: invalid putting");
        }
    }
}

bool Board::is_connected(const Coordinates& coordinates) const
{
    CoordinatesStack checking_list;
    CoordinatesList checked_list;
    bool found = false;

    checking_list.push(coordinates);
    while (not checking_list.empty() and not found) {
        Coordinates current_coordinates = checking_list.top();
        Intersections::const_iterator it =
            mIntersections.find(current_coordinates);

        checked_list.push_back(current_coordinates);
        checking_list.pop();
        if (it->second.dvonn()) {
            found = true;
        } else {
            Direction direction = NORTH_WEST;
            bool stop = false;

            while (not stop) {
                Coordinates destination =
                    current_coordinates.move(1, direction);
                Intersections::const_iterator destination_it =
                    mIntersections.find(destination);

                if (destination.is_valid() and
                    destination_it->second.state() == NO_VACANT) {
                    if (std::find(checked_list.begin(), checked_list.end(),
                                  destination) == checked_list.end()) {
                        checking_list.push(destination);
                    }
                }
                if (direction == WEST) {
                    stop = true;
                } else {
                    direction = next_direction(direction);
                }
            }
        }
    }
    return found;
}

CoordinatesList Board::remove_isolated_stacks()
{
    CoordinatesList list;
    Intersections::const_iterator it = mIntersections.begin();

    while (it != mIntersections.end()) {
        if (it->second.state() == NO_VACANT and not it->second.dvonn()) {
            if (not is_connected(it->second.coordinates())) {
                list.push_back(it->second.coordinates());
            }
        }
        ++it;
    }
    remove_stacks(list);
    return list;
}

void Board::remove_stacks(const CoordinatesList& list)
{
    CoordinatesList::const_iterator it = list.begin();

    while (it != list.end()) {
        Intersections::iterator it2 = mIntersections.find(*it);

        it2->second.remove_stack();
        ++it;
    }
}

bool Board::verify_moving(const Coordinates& origin,
                          const Coordinates& destination) const
{
    if (can_move(origin)) {
        CoordinatesList list = get_stack_possible_move(origin);

        return std::find(list.begin(), list.end(), destination) != list.end();
    } else {
        return false;
    }
}

const char letter_matrix[] = {
    'Z', 'X', 'X', 'A', 'X', 'B', 'X', 'C', 'X', 'D', 'X', 'E',
    'X', 'F', 'X', 'G', 'X', 'H', 'X', 'I', 'X', 'X', 'X', 'X',
    'Z', 'X', 'A', 'X', 'B', 'X', 'C', 'X', 'D', 'X', 'E', 'X',
    'F', 'X', 'G', 'X', 'H', 'X', 'I', 'X', 'J', 'X', 'X', 'X',
    'Z', 'A', 'X', 'B', 'X', 'C', 'X', 'D', 'X', 'E', 'X', 'F',
    'X', 'G', 'X', 'H', 'X', 'I', 'X', 'J', 'X', 'K', 'X', 'X',
    'Z', 'X', 'B', 'X', 'C', 'X', 'D', 'X', 'E', 'X', 'F', 'X',
    'G', 'X', 'H', 'X', 'I', 'X', 'J', 'X', 'K', 'X', 'X', 'X',
    'Z', 'X', 'X', 'C', 'X', 'D', 'X', 'E', 'X', 'F', 'X', 'G',
    'X', 'H', 'X', 'I', 'X', 'J', 'X', 'K', 'X', 'X', 'X', 'X'
};

std::string Board::to_string() const
{
    std::string str;

    str = "                 A           B           C           D           E" \
        "           F           G           H           I           J"  \
        "           K\n";
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 24; ++j) {
            char l = letter_matrix[i * 24 + j];

            if (l != 'X' and l != 'Z') {
                openxum::dvonn::Coordinates coordinates(l, i + 1);
                Intersections::const_iterator it =
                    mIntersections.find(coordinates);

                if (it != mIntersections.end()) {
                    str += it->second.to_string();
                }
            } else if (l == 'Z') {
                std::string s(" ");

                s += '0' + (i + 1);
                s += ' ';
                str += s;
            } else {
                str += "      ";
            }
        }
        str += "\n";
    }
    return str;
}

Color Board::winner_is() const
{
    if (is_finished()) {
        int white_size = get_max_stack_size(WHITE);
        int black_size = get_max_stack_size(BLACK);

        if (white_size > black_size) return WHITE;
        else if (black_size > white_size) return BLACK;
        else return NONE;
    } else {
        throw InvalidWinner("dvonn: game is not finished");
    }
}

}} // namespace openxum dvonn
