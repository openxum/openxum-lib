INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/src ${OPENXUM_COMMON_BINARY_DIR}/src)

SET(DVONN_HPP coordinates.hpp exception.hpp intersection.hpp board.hpp)
SET(DVONN_CPP coordinates.cpp intersection.cpp board.cpp)

ADD_LIBRARY(openxum-dvonn-common SHARED ${DVONN_HPP};${DVONN_CPP})

INSTALL(TARGETS openxum-dvonn-common
  RUNTIME DESTINATION bin
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib)

INSTALL(FILES ${DVONN_HPP} DESTINATION ${OPENXUM_INCLUDE_DIR}/dvonn)