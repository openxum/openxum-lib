/*
 * @file dvonn/intersection.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DVONN_INTERSECTION_HPP
#define _DVONN_INTERSECTION_HPP 1

#include <common/color.hpp>
#include <dvonn/coordinates.hpp>
#include <stack>
#include <string>

using namespace openxum::common;

namespace openxum { namespace dvonn {

enum State { VACANT, NO_VACANT };

class Intersection
{

class Piece
{
public:
    Piece(Color color) : mColor(color), mDvonn(color == RED)
    { }

    Piece(const Piece& piece) : mColor(piece.mColor), mDvonn(piece.mDvonn)
    { }

    Color color() const
    { return mColor; }

    bool dvonn() const
    { return mDvonn; }

private:
    Color mColor;
    bool mDvonn;
};

class Stack : private std::stack < Piece >
{
public:
    Stack() : mDvonn(false)
    { }

    Color color() const
    { return top().color(); }

    void clear()
    {
        while (not empty()) {
            pop();
        }
    }

    bool dvonn() const
    { return mDvonn; }

    bool empty() const
    { return std::stack < Piece >::empty(); }

    void put(Piece piece)
    {
        if (not mDvonn) {
            mDvonn = piece.dvonn();
        }
        push(piece);
    }

    Piece remove_top()
    {
        Piece top = std::stack < Piece >::top();

        pop();
        if (empty()) {
            mDvonn = false;
        }
        return top;
    }

    int size() const
    { return std::stack < Piece >::size(); }

private:
    bool mDvonn;
};

public:
    Intersection(const openxum::dvonn::Coordinates& coordinates) :
        mCoordinates(coordinates), mState(VACANT)
    { }

    Intersection(const openxum::dvonn::Coordinates& coordinates, State state) :
        mCoordinates(coordinates), mState(state)
    { }

    virtual ~Intersection()
    { }

    Color color() const;

    const openxum::dvonn::Coordinates& coordinates() const
    { return mCoordinates; }

    bool dvonn() const
    { return mStack.dvonn(); }

    char letter() const
    { return mCoordinates.letter(); }

    void move_stack_to(Intersection& destination);

    int number() const
    { return mCoordinates.number(); }

    virtual bool operator==(const Intersection& intersection) const
    { mCoordinates == intersection.mCoordinates; }

    void put_piece(Color color)
    {
        mState = NO_VACANT;
        mStack.put(Piece(color));
    }

    void remove_stack()
    {
        mState = VACANT;
        mStack.clear();
    }

    State state() const;

    int size() const
    { return mStack.size(); }

    std::string to_string() const;

private:
    openxum::dvonn::Coordinates mCoordinates;
    State mState;
    Stack mStack;
};

}} // namespace openxum dvonn

#endif
