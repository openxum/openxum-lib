/*
 * @file dvonn/coordinates.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DVONN_COORDINATES_HPP
#define _DVONN_COORDINATES_HPP 1

#include <common/coordinates.hpp>
#include <stack>

namespace openxum { namespace dvonn {

enum Direction { NORTH_WEST, NORTH_EAST, EAST, SOUTH_EAST, SOUTH_WEST, WEST };

class Coordinates : public openxum::common::Coordinates
{
public:
    Coordinates(char letter = 'A', int number = 1) :
        openxum::common::Coordinates(letter, number)
    { }

    virtual ~Coordinates()
    { }

    virtual void decode(const char* msg)
    {
        mLetter = msg[0];
        mNumber = msg[1] - '0';
    }

    virtual void encode(char* msg) const
    {
        msg[0] = mLetter;
        msg[1] = (char)('0'+ mNumber);
        msg[2] = 0;
    }

    virtual int key() const
    { return (mLetter - 'A') + (mNumber - 1) * 11; }

    virtual bool is_valid() const
    { return (mNumber == 1 and mLetter >= 'A' and mLetter <= 'I') or
            (mNumber == 2 and mLetter >= 'A' and mLetter <= 'J') or
            (mNumber == 3 and mLetter >= 'A' and mLetter <= 'K') or
            (mNumber == 4 and mLetter >= 'B' and mLetter <= 'K') or
            (mNumber == 5 and mLetter >= 'C' and mLetter <= 'K'); }

    Coordinates move(int distance, Direction direction) const;
};

typedef std::stack < Coordinates > CoordinatesStack;
typedef std::vector < Coordinates > CoordinatesList;

}} // namespace openxum dvonn

#endif
