/*
 * @file invers/board.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <invers/board.hpp>

using namespace openxum::common;

namespace openxum { namespace invers {

Board::Board(Type type) : mType(type), mRedTileNumber(1), mYellowTileNumber(1)
{
    bool red = true;

    for (int n = 1; n <= 6; ++n) {
        for (char l = 'A'; l <= 'F'; ++l) {
            Coordinates coordinates(l, n);

            mIntersections.insert(
                std::make_pair(coordinates,
                               Intersection(coordinates, red ? RED : YELLOW)));
            red = not red;
        }
        red = not red;
    }
    mCurrentPlayer = (rand() % 1024 < 512) ? RED : YELLOW;
}

Board::Board(Type type, Color player) : mType(type), mCurrentPlayer(player),
                                        mRedTileNumber(1), mYellowTileNumber(1)
{
    bool red = true;

    for (int n = 1; n <= 6; ++n) {
        for (char l = 'A'; l <= 'F'; ++l) {
            Coordinates coordinates(l, n);

            mIntersections.insert(
                std::make_pair(coordinates,
                               Intersection(coordinates, red ? RED : YELLOW)));
            red = not red;
        }
        red = not red;
    }
}

void Board::change_player()
{
    if (mCurrentPlayer == RED) {
        mCurrentPlayer = YELLOW;
    } else {
        mCurrentPlayer = RED;
    }
}

bool Board::exist_intersection(char letter, int number) const
{
    Coordinates coordinates(letter, number);

    if (coordinates.is_valid()) {
        Intersections::const_iterator it = mIntersections.find(coordinates);

        return it != mIntersections.end();
    } else {
        return false;
    }
}

const Intersection* Board::get_intersection(char letter, int number) const
{
    Coordinates coordinates(letter, number);
    Intersections::const_iterator it = mIntersections.find(coordinates);

    if (it != mIntersections.end()) {
        return &(it->second);
    } else {
        return 0;
    }
}

Intersection* Board::get_intersection(char letter, int number)
{
    Coordinates coordinates(letter, number);
    Intersections::iterator it = mIntersections.find(coordinates);

    if (it != mIntersections.end()) {
        return &(it->second);
    } else {
        return 0;
    }
}

bool Board::is_finished(Color color) const
{
    State state = (color == RED ? RED_FULL : YELLOW_FULL);
    bool found = false;

    for (int n = 1; n <= 6 and not found; ++n) {
        for (char l = 'A'; l <= 'F' and not found; ++l) {
            Coordinates coordinates(l, n);
            Intersections::const_iterator it =
                mIntersections.find(coordinates);

            if (it != mIntersections.end()) {
                found = it->second.state() == state;
            }
        }
    }
    return not found;
}

bool Board::is_finished() const
{
    return is_finished(RED) or is_finished(YELLOW);
}

void Board::put_tile(const Coordinates& coordinates, Position position,
                     Color color, Color player)
{
    Color out;

    if (coordinates.letter() != 'X') {
        char letter = coordinates.letter();

        if (position == TOP) {
            out = intersection_state(letter, 6) == RED_FULL ? RED : YELLOW;
            for (int n = 5; n >= 1; --n) {
                Coordinates destination(letter, n + 1);
                Coordinates origin(letter, n);
                Intersections::const_iterator ito =
                    mIntersections.find(origin);
                Intersections::iterator itd =
                    mIntersections.find(destination);

                if (ito != mIntersections.end() and
                    itd != mIntersections.end()) {
                    itd->second.set_state(ito->second.state());
                }
            }
            get_intersection(letter, 1)->set_state(
                (color == RED ? RED_REVERSE : YELLOW_REVERSE));
        } else {
            out = intersection_state(letter, 1) == RED_FULL ? RED : YELLOW;
            for (int n = 1; n < 6; ++n) {
                Coordinates destination(letter, n);
                Coordinates origin(letter, n + 1);
                Intersections::const_iterator ito =
                    mIntersections.find(origin);
                Intersections::iterator itd =
                    mIntersections.find(destination);

                if (ito != mIntersections.end() and
                    itd != mIntersections.end()) {
                    itd->second.set_state(ito->second.state());
                }
            }
            get_intersection(letter, 6)->set_state(
                (color == RED ? RED_REVERSE : YELLOW_REVERSE));
        }
    } else {
        int number = coordinates.number();

        if (position == RIGHT) {
            out = intersection_state('A', number) == RED_FULL ? RED : YELLOW;
            for (char l = 'B'; l <= 'F'; ++l) {
                Coordinates destination(l - 1, number);
                Coordinates origin(l, number);
                Intersections::const_iterator ito =
                    mIntersections.find(origin);
                Intersections::iterator itd =
                    mIntersections.find(destination);

                if (ito != mIntersections.end() and
                    itd != mIntersections.end()) {
                    itd->second.set_state(ito->second.state());
                }
            }
            get_intersection('F', number)->set_state(
                (color == RED ? RED_REVERSE : YELLOW_REVERSE));
        } else {
            out = intersection_state('F', number) == RED_FULL ? RED : YELLOW;
            for (char l = 'E'; l >= 'A'; --l) {
                Coordinates destination(l + 1, number);
                Coordinates origin(l, number);
                Intersections::const_iterator ito =
                    mIntersections.find(origin);
                Intersections::iterator itd =
                    mIntersections.find(destination);

                if (ito != mIntersections.end() and
                    itd != mIntersections.end()) {
                    itd->second.set_state(ito->second.state());
                }
            }
            get_intersection('A', number)->set_state(
                (color == RED ? RED_REVERSE : YELLOW_REVERSE));
        }
    }
    if (color == RED) {
        --mRedTileNumber;
    } else {
        --mYellowTileNumber;
    }
    if (out == RED) {
        ++mRedTileNumber;
    } else {
        ++mYellowTileNumber;
    }
    change_player();
}

std::string Board::to_string() const
{
    std::string str = "    A   B   C   D   E   F\n";

    for (int n = 1; n <= 6; ++n) {
        str += " ";
        str += '0' + n;
        str += " ";
        for (char l = 'A'; l <= 'F'; ++l) {
            Coordinates coordinates(l, n);
            Intersections::const_iterator it =
                mIntersections.find(coordinates);

            if (it != mIntersections.end()) {
                str += it->second.to_string();
            }
        }
        str += "\n";
    }
    return str;
}

Color Board::winner_is() const
{
    if (is_finished(RED)) {
        return RED;
    } else if (is_finished(YELLOW)) {
        return YELLOW;
    } else {
        throw InvalidWinner("invers: game is not finished");
    }
}

/*  - - - - - - - - - - - - - --ooOoo-- - - - - - - - - - - -  */

void Board::get_possible_position(CoordinatesList& top, CoordinatesList& bottom,
                                  CoordinatesList& left, CoordinatesList& right,
                                  Color player)
{
    State state = player == RED ? YELLOW_REVERSE : RED_REVERSE;

    for (int n = 1; n <= 6; ++n) {
        // RIGHT
        {
            Coordinates coordinates('A', n);
            Intersections::const_iterator it =
                mIntersections.find(coordinates);

            if (it != mIntersections.end() and
                it->second.state() != state) {
                right.push_back(Coordinates('X', n));
            }
        }
        // LEFT
        {
            Coordinates coordinates('F', n);
            Intersections::const_iterator it =
                mIntersections.find(coordinates);

            if (it != mIntersections.end() and
                it->second.state() != state) {
                left.push_back(Coordinates('X', n));
            }
        }
    }

    for (char l = 'A'; l <= 'F'; ++l) {
        // BOTTOM
        {
            Coordinates coordinates(l, 1);
            Intersections::const_iterator it =
                mIntersections.find(coordinates);

            if (it != mIntersections.end() and
                it->second.state() != state) {
                bottom.push_back(Coordinates(l, 0));
            }
        }
        // TOP
        {
            Coordinates coordinates(l, 6);
            Intersections::const_iterator it =
                mIntersections.find(coordinates);

            if (it != mIntersections.end() and
                it->second.state() != state) {
                top.push_back(Coordinates(l, 0));
            }
        }
    }
}

}} // namespace openxum invers
