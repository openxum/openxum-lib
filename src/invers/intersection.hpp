/*
 * @file invers/intersection.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _INVERS_INTERSECTION_HPP
#define _INVERS_INTERSECTION_HPP 1

#include <common/color.hpp>
#include <invers/coordinates.hpp>
#include <string>

namespace openxum { namespace invers {

enum State { UNDEFINED = -1, RED_FULL = 0, YELLOW_FULL = 1, RED_REVERSE = 2,
             YELLOW_REVERSE = 3 };

class Intersection
{
public:
    Intersection(const Coordinates& coordinates, common::Color color) :
        mCoordinates(coordinates)
    {
        if (color == common::RED) {
            mState = RED_FULL;
        } else {
            mState = YELLOW_FULL;
        }
    }

    virtual ~Intersection()
    { }

    const Coordinates& coordinates() const
    { return mCoordinates; }

    char letter() const
    { return mCoordinates.letter(); }

    int number() const
    { return mCoordinates.number(); }

    virtual bool operator==(const Intersection& intersection) const
    { mCoordinates == intersection.mCoordinates; }

    State state() const;

    void set_state(State state)
    { mState = state; }

    std::string to_string() const;

private:
    Coordinates mCoordinates;
    State mState;
};

}} // namespace openxum invers

#endif
