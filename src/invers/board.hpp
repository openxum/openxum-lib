/*
 * @file invers/board.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _INVERS_BOARD_HPP
#define _INVERS_BOARD_HPP 1

#include <map>
#include <vector>

#include <invers/coordinates.hpp>
#include <invers/exception.hpp>
#include <invers/intersection.hpp>

namespace openxum { namespace invers {

enum Type { REGULAR, BLITZ };
enum Position { TOP, BOTTOM, LEFT, RIGHT };

class Board
{
public:
    typedef std::map < Coordinates, Intersection > Intersections;

    Board(Type type = REGULAR);
    Board(Type type, common::Color player);

    virtual ~Board()
    { }

    common::Color current_player() const
    { return mCurrentPlayer; }
    bool exist_intersection(char letter, int number) const;
    State intersection_state(char letter, int number) const
    { return get_intersection(letter, number)->state(); }
    const Intersections& intersections() const
    { return mIntersections; }
    bool is_finished() const;
    void put_tile(const Coordinates& coordinates, Position position,
                  common::Color color, common::Color player);
    int red_tile_number() const
    { return mRedTileNumber; }
    std::string to_string() const;
    common::Color winner_is() const;
    int yellow_tile_number() const
    { return mYellowTileNumber; }

    // strategy
    void get_possible_position(CoordinatesList& top, CoordinatesList& bottom,
                               CoordinatesList& left, CoordinatesList& right,
                               common::Color player);

private:
    void change_player();
    const Intersection* get_intersection(char letter, int number) const;
    Intersection* get_intersection(char letter, int number);
    bool is_finished(common::Color color) const;

    Intersections mIntersections;
    Type mType;

    common::Color mCurrentPlayer;
    int mRedTileNumber;
    int mYellowTileNumber;
};

}} // namespace openxum invers

#endif
