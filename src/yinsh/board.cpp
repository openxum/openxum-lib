/*
 * @file yinsh/board.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <yinsh/board.hpp>

using namespace openxum::common;

namespace openxum { namespace yinsh {

const char Board::begin_letter[] = { 'B', 'A', 'A', 'A', 'A', 'B', 'B', 'C',
                                     'D', 'E', 'G' };
const char Board::end_letter[] = { 'E', 'G', 'H', 'I', 'J', 'J', 'K', 'K', 'K',
                                   'K', 'J' };

const int Board::begin_number[] = { 2, 1, 1, 1, 1, 2, 2, 3, 4, 5, 7 };
const int Board::end_number[] = { 5, 7, 8, 9, 10, 10, 11, 11, 11, 11, 10 };

const char Board::begin_diagonal_letter[] = { 'B', 'A', 'A', 'A', 'A', 'B',
                                              'B', 'C', 'D', 'E', 'G'};
const char Board::end_diagonal_letter[] = { 'E', 'G', 'H', 'I', 'J', 'J',
                                            'K', 'K', 'K', 'K', 'J' };

const int Board::begin_diagonal_number[] = { 7, 5, 4, 3, 2, 2, 1, 1, 1, 1, 2 };
const int Board::end_diagonal_number[] = { 10, 11, 11, 11, 11, 10, 10, 9, 8,
                                           7, 5 };

const unsigned int initial_marker_number = 51;

Board::Board(Type type) : mType(type),
                          mPhase(PUT_RING),
                          mRemovedBlackRingNumber(0),
                          mRemovedWhiteRingNumber(0),
                          mMarkerNumber(initial_marker_number)
{
    for (char l = 'A'; l < 'L'; ++l) {
        for (int n = begin_number[l - 'A']; n <= end_number[l - 'A']; ++n) {
            openxum::yinsh::Coordinates coordinates(l, n);

            mIntersections.insert(
                std::make_pair(coordinates, Intersection(coordinates)));
        }
    }
    mCurrentColor = (rand() % 1024 < 512) ? BLACK : WHITE;
}

Board::Board(Type type, Color color) : mType(type),
                                       mPhase(PUT_RING),
                                       mRemovedBlackRingNumber(0),
                                       mRemovedWhiteRingNumber(0),
                                       mMarkerNumber(initial_marker_number),
                                       mCurrentColor(color)
{
    for (char l = 'A'; l < 'L'; ++l) {
        for (int n = begin_number[l - 'A']; n <= end_number[l - 'A']; ++n) {
            openxum::yinsh::Coordinates coordinates(l, n);

            mIntersections.insert(
                std::make_pair(coordinates, Intersection(coordinates)));
        }
    }
}

Board::Board(const Board& board) : mType(board.mType),
                                   mPhase(board.mPhase),
                                   mRemovedBlackRingNumber(
                                       board.mRemovedBlackRingNumber),
                                   mRemovedWhiteRingNumber(
                                       board.mRemovedWhiteRingNumber),
                                   mPlacedWhiteRingCoordinates(
                                       board.mPlacedWhiteRingCoordinates),
                                   mPlacedBlackRingCoordinates(
                                       board.mPlacedBlackRingCoordinates),
                                   mMarkerNumber(board.mMarkerNumber),
                                   mCurrentColor(board.mCurrentColor)
{

    for (char l = 'A'; l < 'L'; ++l) {
        for (int n = begin_number[l - 'A']; n <= end_number[l - 'A']; ++n) {
            openxum::yinsh::Coordinates coordinates(l, n);

            mIntersections.insert(
                std::make_pair(coordinates,
                               Intersection(*board.get_intersection(l, n))));
        }
    }
}

void Board::buid_row(char letter, int number, State state, bool& start,
                     Rows& rows, Row& row) const
{
    openxum::yinsh::Coordinates coordinates(letter, number);
    Intersections::const_iterator intersection =
        mIntersections.find(coordinates);

    if (not start and intersection->second.state() == state) {
        start = true;
        row.push_back(coordinates);
    } else if (start and intersection->second.state() == state) {
        row.push_back(coordinates);
    } else if (start and intersection->second.state() != state) {
        if (row.size() >= 5) {
            rows.push_back(row);
        }
        start = false;
        row.clear();
    }
}

void Board::change_color()
{
    if (mCurrentColor == WHITE) {
        mCurrentColor = BLACK;
    } else {
        mCurrentColor = WHITE;
    }
}

bool Board::exist_intersection(char letter, int number) const
{
    openxum::yinsh::Coordinates coordinates(letter, number);

    if (coordinates.is_valid()) {
        Intersections::const_iterator it = mIntersections.find(coordinates);

        return it != mIntersections.end();
    } else {
        return false;
    }
}

void Board::flip(char letter, int number)
{
    openxum::yinsh::Coordinates coordinates(letter, number);
    Intersections::iterator it = mIntersections.find(coordinates);

    if (it != mIntersections.end()) {
        it->second.flip();
    }
}

void Board::flip(const Intersection& origin,
                 const Intersection& destination)
{
    if (origin.letter() == destination.letter()) {
        if (origin.number() < destination.number()) {
            int n = origin.number() + 1;

            while (n < destination.number()) {
                flip(origin.letter(), n);
                ++n;
            }
        } else {
            int n = origin.number() - 1;

            while (n > destination.number()) {
                flip(origin.letter(), n);
                --n;
            }
        }
    } else if (origin.number() == destination.number()) {
        if (origin.letter() < destination.letter()) {
            char l = origin.letter() + 1;

            while(l < destination.letter()) {
                flip(l, origin.number());
                ++l;
            }
        } else {
            char l = origin.letter() - 1;

            while (l > destination.letter()) {
                flip(l, origin.number());
                --l;
            }
        }
    } else {
        if (origin.letter() < destination.letter()) {
            int n = origin.number() + 1;
            char l = origin.letter() + 1;

            while (l < destination.letter()) {
                flip(l, n);
                ++l;
                ++n;
            }
        } else {
            int n = origin.number() - 1;
            char l = origin.letter() - 1;

            while (l > destination.letter()) {
                flip(l, n);
                --l;
                --n;
            }
        }
    }
}

const Intersection* Board::get_intersection(char letter, int number) const
{
    openxum::yinsh::Coordinates coordinates(letter, number);
    Intersections::const_iterator it = mIntersections.find(coordinates);

    if (it != mIntersections.end()) {
        return &(it->second);
    } else {
        return 0;
    }
}

SeparatedRows Board::get_rows(Color color) const
{
    Rows rows;
    State state = (color == BLACK)?BLACK_MARKER:WHITE_MARKER;

    for (int n = 1; n <= 11; ++n) {
        bool start = false;
        Row row;

        for (char l = begin_letter[n - 1]; l <= end_letter[n - 1]; ++l) {
            buid_row(l, n, state, start, rows, row);
        }
        if (row.size() >= 5) {
            rows.push_back(row);
        }
    }

    for (char l = 'A'; l <= 'K'; ++l) {
        bool start = false;
        Row row;

        for (int n = begin_number[l - 'A']; n <= end_number[l - 'A']; ++n) {
            buid_row(l, n, state, start, rows, row);
        }
        if (row.size() >= 5) {
            rows.push_back(row);
        }
    }

    for (int i = 0; i <= 10; ++i) {
        bool start = false;
        Row row;
        int n = begin_diagonal_number[i];
        char l = begin_diagonal_letter[i];

        while (l <= end_diagonal_letter[i] and n <= end_diagonal_number[i]) {
            buid_row(l, n, state, start, rows, row);
            ++l;
            ++n;
        }
        if (row.size() >= 5) {
            rows.push_back(row);
        }
    }

    SeparatedRows srows;

    if (not rows.empty()) {
        Rows list;

        list.push_back(rows.back());
        srows.push_back(list);
        rows.pop_back();
        while (not rows.empty()) {
            Row row = rows.back();
            bool found = false;
            SeparatedRows::iterator it = srows.begin();

            while (it != srows.end() and not found) {
                Rows::const_iterator itr = it->begin();

                while (itr != it->end() and not found) {
                    if (not row.is_separated(*itr)) {
                        it->push_back(row);
                        found = true;
                    } else {
                        ++itr;
                    }
                }
                ++it;
            }
            if (not found) {
                Rows list;

                list.push_back(row);
                srows.push_back(list);
            }
            rows.pop_back();
        }
    }

    return srows;
}

bool Board::is_finished() const
{
    if (mType == BLITZ) {
        return mRemovedBlackRingNumber == 1 or mRemovedWhiteRingNumber == 1 or
            mMarkerNumber == 0;
    } else { // type = REGULAR
        return mRemovedBlackRingNumber == 3 or mRemovedWhiteRingNumber == 3 or
            mMarkerNumber == 0;
    }
}

void Board::move_ring(const openxum::yinsh::Coordinates& origin,
                      const openxum::yinsh::Coordinates& destination)
{
    Intersections::iterator ito = mIntersections.find(origin);
    Intersections::iterator itd = mIntersections.find(destination);

    if (ito == mIntersections.end()) {
        throw InvalidMoving("yinsh: invalid origin coordinates");
    }
    if (itd == mIntersections.end()) {
        throw InvalidMoving("yinsh: invalid destination coordinates");
    }
    if (itd->second.state() != VACANT){
        throw InvalidMoving("yinsh: destination not vacant");
    }
    if (not verify_moving(ito->second, itd->second)) {
        throw InvalidMoving("yinsh: invalid moving");
    }

    Color color = ito->second.color();

    ito->second.remove_ring();
    itd->second.put_ring(color);
    flip(ito->second, itd->second);

    if (color == BLACK) {
        remove_black_ring(origin);
        mPlacedBlackRingCoordinates.push_back(destination);
    } else {
        remove_white_ring(origin);
        mPlacedWhiteRingCoordinates.push_back(destination);
    }
    mPhase = REMOVE_ROWS_AFTER;
}

void Board::put_marker(const openxum::yinsh::Coordinates& coordinates,
                       Color color)
{
    if (mMarkerNumber > 0) {
        Intersections::iterator it = mIntersections.find(coordinates);

        if (it != mIntersections.end()) {
            it->second.put_marker(color);
        } else {
            throw InvalidPutting("yinsh: invalid coordinates");
        }
        --mMarkerNumber;
    } else {
        throw InvalidPutting("yinsh: the number of markers is null");
    }
    mPhase = MOVE_RING;
}

void Board::put_ring(const openxum::yinsh::Coordinates& coordinates,
                     Color color)
{
    if (color != mCurrentColor) {
        throw InvalidPutting("yinsh: invalid color");
    }
    if ((color == BLACK and mPlacedBlackRingCoordinates.size() == 5) or
        (color == WHITE and mPlacedWhiteRingCoordinates.size() == 5)) {
        throw InvalidPutting("yinsh: too many rings");
    }

    Intersections::iterator it = mIntersections.find(coordinates);

    if (it != mIntersections.end()) {
        it->second.put_ring(color);
    } else {
        throw InvalidPutting("yinsh: invalid coordinates");
    }
    if (color == WHITE) {
        mPlacedWhiteRingCoordinates.push_back(coordinates);
    } else {
        mPlacedBlackRingCoordinates.push_back(coordinates);
    }
    if (mPlacedBlackRingCoordinates.size() == 5 and
        mPlacedWhiteRingCoordinates.size() == 5) {
        mPhase = REMOVE_ROWS_BEFORE;
    }
    change_color();
}

void Board::remove_black_ring(const openxum::yinsh::Coordinates& coordinates)
{
    CoordinatesList::iterator it = mPlacedBlackRingCoordinates.begin();
    bool found = false;

    while (it != mPlacedBlackRingCoordinates.end() and not found) {
        found = *it == coordinates;
        if (not found) {
            ++it;
        }
    }
    if (found) {
        mPlacedBlackRingCoordinates.erase(it);
    }
}

void Board::remove_marker(char letter, int number)
{
    openxum::yinsh::Coordinates coordinates(letter, number);
    Intersections::iterator it = mIntersections.find(coordinates);

    if (it != mIntersections.end()) {
        it->second.remove_marker();
        ++mMarkerNumber;
    } else {
        throw InvalidRemovingRow("yinsh: invalid coordinates");
    }
}

void Board::remove_no_row()
{
    if (mPhase == REMOVE_ROWS_AFTER) {
        change_color();
        mPhase = REMOVE_ROWS_BEFORE;
    } else {
        mPhase = PUT_MARKER;
    }
}

void Board::remove_rows(Rows& rows, Color color)
{
    Rows::const_iterator it = rows.begin();

    while (it != rows.end()) {
        const Row& row = *it;
        Row::const_iterator itr = row.begin();

        if (row.size() != 5) {
            throw InvalidRemovingRow("yinsh: invalid row length");
        }

        while (itr != row.end()) {
            remove_marker(itr->letter(), itr->number());
            ++itr;
        }
        ++it;
    }
    if (mPhase == REMOVE_ROWS_AFTER) {
        mPhase = REMOVE_RING_AFTER;
    } else {
        mPhase = REMOVE_RING_BEFORE;
    }
}

void Board::remove_ring(const openxum::yinsh::Coordinates& coordinates,
                        Color color)
{
    Intersections::iterator it = mIntersections.find(coordinates);

    if (it != mIntersections.end()) {
        if (it->second.color() == color) {
            it->second.remove_ring_board();
        } else {
            throw InvalidRemovingRing("yinsh: invalid color");
        }
    } else {
        throw InvalidRemovingRing("yinsh: invalid coordinates");
    }

    if (color == BLACK) {
        remove_black_ring(coordinates);
        ++mRemovedBlackRingNumber;
    } else {
        remove_white_ring(coordinates);
        ++mRemovedWhiteRingNumber;
    }
    if (mPhase == REMOVE_RING_AFTER) {
        mPhase = REMOVE_ROWS_BEFORE;
        change_color();
    } else {
        mPhase = PUT_MARKER;
    }
}

void Board::remove_white_ring(const openxum::yinsh::Coordinates& coordinates)
{
    CoordinatesList::iterator it = mPlacedWhiteRingCoordinates.begin();
    bool found = false;

    while (it != mPlacedWhiteRingCoordinates.end() and not found) {
        found = *it == coordinates;
        if (not found) {
            ++it;
        }
    }
    if (found) {
        mPlacedWhiteRingCoordinates.erase(it);
    }
}

const char letter_matrix[] = {
    'X', 'X', 'X', 'Z', 'X', 'E', 'X', 'G', 'X', 'X', 'X', 'X',
    'X', 'X', 'Z', 'X', 'D', 'X', 'F', 'X', 'H', 'X', 'X', 'X',
    'X', 'Z', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I', 'X', 'X',
    'X', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X', 'J', 'X',
    'X', 'Z', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I', 'X', 'X',
    'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X', 'J', 'X',
    'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I', 'X', 'K',
    'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X', 'J', 'X',
    'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I', 'X', 'K',
    'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X', 'J', 'X',
    'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I', 'X', 'K',
    'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X', 'J', 'X',
    'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I', 'X', 'K',
    'X', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X', 'J', 'X',
    'X', 'Z', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I', 'X', 'X',
    'X', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X', 'J', 'X',
    'X', 'X', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I', 'X', 'X',
    'X', 'X', 'X', 'X', 'D', 'X', 'F', 'X', 'H', 'X', 'X', 'X',
    'X', 'X', 'X', 'X', 'X', 'E', 'X', 'G', 'X', 'X', 'X', 'X'
};

const char number_matrix[] = {
    0, 0, 0, 0, 0, 10, 0, 11, 0, 0, 0, 0,
    0, 0, 0, 0, 9, 0, 10, 0, 11, 0, 0, 0,
    0, 0, 0, 8, 0, 9, 0, 10, 0, 11, 0, 0,
    0, 0, 7, 0, 8, 0, 9, 0, 10, 0, 11, 0,
    0, 0, 0, 7, 0, 8, 0, 9, 0, 10, 0, 0,
    0, 0, 6, 0, 7, 0, 8, 0, 9, 0, 10, 0,
    0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 10,
    0, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0,
    0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9,
    0, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0,
    0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8,
    0, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0,
    0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7,
    0, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0,
    0, 0, 0, 2, 0, 3, 0, 4, 0, 5, 0, 0,
    0, 0, 1, 0, 2, 0, 3, 0, 4, 0, 5, 0,
    0, 0, 0, 1, 0, 2, 0, 3, 0, 4, 0, 0,
    0, 0, 0, 0, 1, 0, 2, 0, 3, 0, 0, 0,
    0, 0, 0, 0, 0, 1, 0, 2, 0, 0, 0, 0
};

std::string Board::to_string() const
{
    std::string str("                       10        11\n");
    unsigned int k = 9;

    for (int j = 0; j < 19; ++j) {
        for (int i = 0; i < 12; ++i) {
            char l = letter_matrix[i + 12 * j];
            int n = number_matrix[i + 12 * j];

            if (l != 'X' and l != 'Z') {
                openxum::yinsh::Coordinates coordinates(l, n);
                Intersections::const_iterator it =
                    mIntersections.find(coordinates);

                if (it != mIntersections.end()) {
                    str += it->second.to_string();
                }
            } else if (l == 'Z') {
                std::string s("    ");

                s += '0' + k;
                str += s;
                --k;
            } else {
                str += "     ";
            }
        }
        str += "\n";
    }
    str += "      A    B    C    D    E    F    G    H    I    J    K\n";
    return str;
}

void Board::verify_intersection(char letter, int number,
                                bool& ok, bool& no_vacant) const
{
    openxum::yinsh::Coordinates coordinates(letter, number);
    Intersections::const_iterator it = mIntersections.find(coordinates);

    if (it != mIntersections.end()) {
        State state = it->second.state();

        if (state == BLACK_RING or state == WHITE_RING) {
            no_vacant = false; // if ring is presenter after row of markers
            ok = false;
        } else if (state == BLACK_MARKER or state == WHITE_MARKER) {
            no_vacant = true;
        } else if (state == VACANT and no_vacant) {
            ok = false;
        }
    }
}

void Board::verify_intersection_in_row(char letter, int number,
                                       Color color,
                                       bool& ok) const
{
    openxum::yinsh::Coordinates coordinates(letter, number);
    Intersections::const_iterator it = mIntersections.find(coordinates);

    if (it != mIntersections.end()) {
        if (it->second.color() != color) {
            ok = false;
        }
    }
}

bool Board::verify_moving(const openxum::yinsh::Coordinates& origin,
                          const openxum::yinsh::Coordinates& destination) const
{
    Intersections::const_iterator ito = mIntersections.find(origin);
    Intersections::const_iterator itd = mIntersections.find(destination);

    if (ito != mIntersections.end() and itd != mIntersections.end()) {
        return verify_moving(ito->second, itd->second);
    } else {
        return false;
    }
}

bool Board::verify_moving(const Intersection& origin,
                          const Intersection& destination) const
{
    bool ok = true;

    if (origin.state() != BLACK_MARKER_RING and
        origin.state() != WHITE_MARKER_RING) {
        throw InvalidMoving("yinsh: no ring at origin");
    }

    if (origin == destination or destination.state() != VACANT) {
        return false;
    }

    if (origin.letter() == destination.letter()) {
        if (origin.number() < destination.number()) {
            int n = origin.number() + 1;
            bool no_vacant = false;

            while (n < destination.number() and ok) {
                verify_intersection(origin.letter(), n, ok, no_vacant);
                ++n;
            }
        } else {
            int n = origin.number() - 1;
            bool no_vacant = false;

            while (n > destination.number() and ok) {
                verify_intersection(origin.letter(), n, ok, no_vacant);
                --n;
            }
        }
    } else if (origin.number() == destination.number()) {
        if (origin.letter() < destination.letter()) {
            char l = origin.letter() + 1;
            bool no_vacant = false;

            while (l < destination.letter() and ok) {
                verify_intersection(l, origin.number(), ok, no_vacant);
                ++l;
            }
        } else {
            char l = origin.letter() - 1;
            bool no_vacant = false;

            while (l > destination.letter() and ok) {
                verify_intersection(l, origin.number(), ok, no_vacant);
                --l;
            }
        }
    } else {
        if (origin.letter() - destination.letter() ==
            origin.number() - destination.number()) {
            if (origin.letter() < destination.letter()) {
                int n = origin.number() + 1;
                char l = origin.letter() + 1;
                bool no_vacant = false;

                while (l < destination.letter() and ok) {
                    verify_intersection(l, n, ok, no_vacant);
                    ++l;
                    ++n;
                }
            } else {
                int n = origin.number() - 1;
                char l = origin.letter() - 1;
                bool no_vacant = false;

                while (l > destination.letter() and ok) {
                    verify_intersection(l, n, ok, no_vacant);
                    --l;
                    --n;
                }
            }
        } else {
            ok = false;
        }
    }

    return ok;
}

bool Board::verify_row(const Intersection& begin,
                       const Intersection& end,
                       Color color) const
{
    bool ok = true;

    if (begin.letter() == end.letter()) {
        if (begin.number() < end.number()) {
            int n = begin.number() + 1;

            while (n < end.number() and ok) {
                verify_intersection_in_row(begin.letter(), n, color, ok);
                ++n;
            }
        } else {
            int n = begin.number() - 1;

            while (n > end.number() and ok) {
                verify_intersection_in_row(begin.letter(), n, color, ok);
                --n;
            }
        }
    } else if (begin.number() == end.number()) {
        if (begin.letter() < end.letter()) {
            char l = begin.letter() + 1;

            while (l < end.letter() and ok) {
                verify_intersection_in_row(l, begin.number(), color, ok);
                ++l;
            }
        } else {
            char l = begin.letter() - 1;

            while (l > end.letter() and ok) {
                verify_intersection_in_row(l, begin.number(), color, ok);
                --l;
            }
        }
    } else {
        if (begin.letter() - end.letter() ==
            begin.number() - end.number()) {
            if (begin.letter() < end.letter()) {
                int n = begin.number() + 1;
                char l = begin.letter() + 1;

                while (l < end.letter() and ok) {
                    verify_intersection_in_row(l, n, color, ok);
                    ++l;
                    ++n;
                }
            } else {
                int n = begin.number() - 1;
                char l = begin.letter() - 1;

                while (l > end.letter() and ok) {
                    verify_intersection_in_row(l, n, color, ok);
                    --l;
                    --n;
                }
            }
        } else {
            ok = false;
        }
    }
    return ok;
}

Color Board::winner_is() const
{
    if (is_finished()) {
        if (mType == REGULAR) {
            if (mRemovedBlackRingNumber == 3 or
                mRemovedBlackRingNumber > mRemovedWhiteRingNumber) {
                return BLACK;
            } else if (mRemovedWhiteRingNumber == 3 or
                mRemovedBlackRingNumber < mRemovedWhiteRingNumber) {
                return WHITE;
            } else {
                return NONE;
            }
        } else {
            if (mRemovedBlackRingNumber == 1) {
                return BLACK;
            } else if (mRemovedWhiteRingNumber == 1) {
                return WHITE;
            } else {
                return NONE;
            }
        }
    } else {
        throw InvalidWinner("yinsh: game is not finished");
    }
}

/*  - - - - - - - - - - - - - --ooOoo-- - - - - - - - - - - -  */

CoordinatesList Board::get_free_intersections() const
{
    CoordinatesList list;
    Intersections::const_iterator it = mIntersections.begin();

    while (it != mIntersections.end()) {
        if (it->second.state() == VACANT) {
            list.push_back(it->first);
        }
        ++it;
    }
    return list;
}

PossibleMovingList Board::get_possible_moving_list(
    const openxum::yinsh::Coordinates& origin, Color color, bool control) const
{
    PossibleMovingList list;
    Intersections::const_iterator it = mIntersections.find(origin);

    if (it == mIntersections.end()) {
        throw InvalidMoving("yinsh: invalid coordinates");
    }

    if (control and
        not((it->second.state() == BLACK_MARKER_RING and color == BLACK) or
            (it->second.state() == WHITE_MARKER_RING and color == WHITE))) {
        throw InvalidMoving("yinsh: invalid color");
    }

    // letter + number increase
    {
        bool ok = true;
        int n = it->second.number() + 1;
        bool no_vacant = false;

        while (n <= end_number[it->second.letter() - 'A'] and ok) {
            verify_intersection(it->second.letter(), n, ok, no_vacant);
            if ((ok and not no_vacant) or (not ok and no_vacant)) {
                list.push_back(get_intersection(it->second.letter(), n));
            }
            ++n;
        }
    }
    // letter + number decrease
    {
        bool ok = true;
        int n = it->second.number() - 1;
        bool no_vacant = false;

        while (n >= begin_number[it->second.letter() - 'A'] and ok) {
            verify_intersection(it->second.letter(), n, ok, no_vacant);
            if ((ok and not no_vacant) or (not ok and no_vacant)) {
                list.push_back(get_intersection(it->second.letter(), n));
            }
            --n;
        }
    }
    // number + letter increase
    {
        bool ok = true;
        char l = it->second.letter() + 1;
        bool no_vacant = false;

        while (l <= end_letter[it->second.number() - 1] and ok) {
            verify_intersection(l, it->second.number(), ok, no_vacant);
            if ((ok and not no_vacant) or (not ok and no_vacant)) {
                list.push_back(get_intersection(l, it->second.number()));
            }
            ++l;
        }
    }
    // number + letter decrease
    {
        bool ok = true;
        char l = it->second.letter() - 1;
        bool no_vacant = false;

        while (l >= begin_letter[it->second.number() - 1] and ok) {
            verify_intersection(l, it->second.number(), ok, no_vacant);
            if ((ok and not no_vacant) or (not ok and no_vacant)) {
                list.push_back(get_intersection(l, it->second.number()));
            }
            --l;
        }
    }
    // number increase + letter increase
    {
        bool ok = true;
        int n = it->second.number() + 1;
        char l = it->second.letter() + 1;
        bool no_vacant = false;

        while (n <= end_number[l - 'A'] and
               l <= end_letter[n - 1] and ok) {
            verify_intersection(l, n, ok, no_vacant);
            if ((ok and not no_vacant) or (not ok and no_vacant)) {
                list.push_back(get_intersection(l, n));
            }
            ++l;
            ++n;
        }
    }
    // number decrease + letter decrease
    {
        bool ok = true;
        int n = it->second.number() - 1;
        char l = it->second.letter() - 1;
        bool no_vacant = false;

        while (n >= begin_number[l - 'A'] and
               l >= begin_letter[n - 1] and ok) {
            verify_intersection(l, n, ok, no_vacant);
            if ((ok and not no_vacant) or (not ok and no_vacant)) {
                list.push_back(get_intersection(l, n));
            }
            --l;
            --n;
        }
    }

    return list;
}

}} // namespace openxum yinsh
