/*
 * @file yinsh/row.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _YINSH_ROW_HPP
#define _YINSH_ROW_HPP 1

#include <string>
#include <vector>

#include <yinsh/coordinates.hpp>

namespace openxum { namespace yinsh {

class Row : public std::vector < Coordinates >
{
public:
    Row()
    { }

    Row(const Coordinates& begin, const Coordinates& end);

    virtual ~Row()
    { }

    bool belong(const Coordinates& coordinates) const;
    void decode(const char* msg);
    void encode(char* msg) const;
    bool is_separated(const Row& row) const;
    std::string to_string() const;

private:
    void build(const Coordinates& begin, const Coordinates& end);
};

typedef std::vector < Row > Rows;

typedef std::vector < Rows > SeparatedRows;

}} // namespace openxum yinsh

#endif
