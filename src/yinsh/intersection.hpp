/*
 * @file yinsh/intersection.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _YINSH_INTERSECTION_HPP
#define _YINSH_INTERSECTION_HPP 1

#include <common/color.hpp>
#include <yinsh/coordinates.hpp>
#include <string>

namespace openxum { namespace yinsh {

enum State { VACANT, BLACK_MARKER, WHITE_MARKER, BLACK_RING, WHITE_RING,
             BLACK_MARKER_RING, WHITE_MARKER_RING };

class Intersection
{
public:
    Intersection(const openxum::yinsh::Coordinates& coordinates) :
        mCoordinates(coordinates), mState(VACANT)
    { }

    virtual ~Intersection()
    { }

    common::Color color() const;
    void flip();

    const openxum::yinsh::Coordinates& coordinates() const
    { return mCoordinates; }

    char letter() const
    { return mCoordinates.letter(); }

    void move_ring();

    int number() const
    { return mCoordinates.number(); }

    void put_marker(common::Color color);
    void put_marker_debug(common::Color color);
    void put_ring(common::Color color);

    virtual bool operator==(const Intersection& intersection) const
    { mCoordinates == intersection.mCoordinates; }

    void remove_marker();
    void remove_ring();
    void remove_ring_board();
    State state() const;
    std::string to_string() const;

private:
    openxum::yinsh::Coordinates mCoordinates;
    State mState;
};

}} // namespace openxum yinsh

#endif
