/*
 * @file yinsh/intersection.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <yinsh/exception.hpp>
#include <yinsh/intersection.hpp>

using namespace openxum::common;

namespace openxum { namespace yinsh {

Color Intersection::color() const
{
    if (mState == VACANT) {
        throw InvalidColor("yinsh: no color");
    }

    if (mState == BLACK_RING or mState == BLACK_MARKER or
        mState == BLACK_MARKER_RING) {
        return BLACK;
    } else {
        return WHITE;
    }
}

void Intersection::flip()
{
    if (mState == BLACK_MARKER) {
        mState = WHITE_MARKER;
    } else if (mState == WHITE_MARKER) {
        mState = BLACK_MARKER;
    }
}

void Intersection::put_marker(Color color)
{
    if (color == BLACK) {
        if (mState == BLACK_RING) {
            mState = BLACK_MARKER_RING;
        } else {
            throw InvalidMoving("yinsh: invalid color marker");
        }
    } else {
        if (mState == WHITE_RING) {
            mState = WHITE_MARKER_RING;
        } else {
            throw InvalidMoving("yinsh: invalid color marker");
        }
    }
}

void Intersection::put_marker_debug(Color color)
{
    if (color == BLACK) {
        mState = BLACK_MARKER;
    } else {
        mState = WHITE_MARKER;
    }
}

void Intersection::put_ring(Color color)
{
    if (color == BLACK) {
        mState = BLACK_RING;
    } else {
        mState = WHITE_RING;
    }
}

void Intersection::remove_marker()
{
    mState = VACANT;
}

void Intersection::remove_ring()
{
    if (mState == BLACK_MARKER_RING or mState == WHITE_MARKER_RING) {
        if (mState == BLACK_MARKER_RING) {
            mState = BLACK_MARKER;
        } else {
            mState = WHITE_MARKER;
        }
    } else {
        throw InvalidMoving("yinsh: invalid ring");
    }
}

void Intersection::remove_ring_board()
{
    mState = VACANT;
}

State Intersection::state() const
{
    return mState;
}

std::string Intersection::to_string() const
{
    switch (mState) {
    case BLACK_MARKER: return "[BM ]";
    case WHITE_MARKER: return "[WM ]";
    case BLACK_RING: return "[BR ]";
    case WHITE_RING: return "[WR ]";
    case BLACK_MARKER_RING: return "[BMR]";
    case WHITE_MARKER_RING: return "[WMR]";
    default: return "[V  ]";
    }
}

}} // namespace openxum yinsh
