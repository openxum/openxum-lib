/*
 * @file yinsh/board.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _YINSH_HPP
#define _YINSH_HPP 1

#include <map>
#include <vector>

#include <yinsh/coordinates.hpp>
#include <yinsh/exception.hpp>
#include <yinsh/intersection.hpp>
#include <yinsh/row.hpp>

namespace openxum { namespace yinsh {

enum Type { REGULAR, BLITZ };

typedef std::vector < const Intersection* > PossibleMovingList;

class Board
{
public:
    typedef std::map < openxum::yinsh::Coordinates,
                       Intersection > Intersections;

    enum Phase { PUT_RING, PUT_MARKER, MOVE_RING, REMOVE_ROWS_AFTER,
                 REMOVE_RING_AFTER, REMOVE_ROWS_BEFORE, REMOVE_RING_BEFORE };

    Board(Type type = REGULAR);
    Board(Type type, common::Color color);
    Board(const Board& board);

    virtual ~Board()
    { }

    unsigned int available_marker_number() const
    { return mMarkerNumber; }
    common::Color current_color() const
    { return mCurrentColor; }
    bool exist_intersection(char letter, int number) const;
    const CoordinatesList& get_placed_ring_coordinates(
        common::Color color) const
    { return (color == common::BLACK) ? mPlacedBlackRingCoordinates :
            mPlacedWhiteRingCoordinates; }
    SeparatedRows get_rows(common::Color color) const;
    State intersection_state(char letter, int number) const
    { return get_intersection(letter, number)->state(); }
    const Intersections& intersections() const
    { return mIntersections; }
    bool is_finished() const;
    bool is_initialized() const
    { return mPlacedBlackRingCoordinates.size() == 5 and
            mPlacedWhiteRingCoordinates.size() == 5; }
    void move_ring(const openxum::yinsh::Coordinates& origin,
                   const openxum::yinsh::Coordinates& destination);
    Phase phase() const
    { return mPhase; }
    void put_marker(const openxum::yinsh::Coordinates& coordinates,
                    common::Color color);
    void put_ring(const openxum::yinsh::Coordinates& coordinates,
                  common::Color color);
    void remove_no_row();
    void remove_rows(Rows& rows, common::Color color);
    void remove_ring(const openxum::yinsh::Coordinates& coordinates,
                     common::Color color);
    unsigned int removed_ring_number(common::Color color) const
    { return (color == common::BLACK) ? mRemovedBlackRingNumber :
            mRemovedWhiteRingNumber; }
    std::string to_string() const;
    Type type() const
    { return mType; }
    bool verify_moving(const openxum::yinsh::Coordinates& origin,
                       const openxum::yinsh::Coordinates& destination) const;
    common::Color winner_is() const;

    // STRATEGY
    CoordinatesList get_free_intersections() const;
    PossibleMovingList get_possible_moving_list(
        const openxum::yinsh::Coordinates& coordinates, common::Color color,
        bool control = true) const;

    // Constantes
    static const char begin_letter[];
    static const char end_letter[];
    static const int begin_number[];
    static const int end_number[];
    static const char begin_diagonal_letter[];
    static const char end_diagonal_letter[];
    static const int begin_diagonal_number[];
    static const int end_diagonal_number[];

private:
    void buid_row(char letter, int number, State state, bool& start,
                  Rows& rows, Row& row) const;
    void change_color();
    void flip(char letter, int number);
    void flip(const Intersection& origin,
              const Intersection& destination);
    const Intersection* get_intersection(char letter, int number) const;
    void remove_black_ring(const openxum::yinsh::Coordinates& coordinates);
    void remove_white_ring(const openxum::yinsh::Coordinates& coordinates);
    void remove_marker(char letter, int number);
    void verify_intersection(char letter, int number,
                             bool& ok, bool& no_vacant) const;
    void verify_intersection_in_row(char letter, int number,
                                    common::Color color, bool& ok) const;
    bool verify_moving(const Intersection& origin,
                       const Intersection& destination) const;
    bool verify_row(const Intersection& begin,
                    const Intersection& end,
                    common::Color color) const;

    Intersections mIntersections;
    Type mType;
    common::Color mCurrentColor;
    Phase mPhase;
    CoordinatesList mPlacedWhiteRingCoordinates;
    CoordinatesList mPlacedBlackRingCoordinates;
    unsigned int mRemovedBlackRingNumber;
    unsigned int mRemovedWhiteRingNumber;
    unsigned int mMarkerNumber;
};

}} // namespace openxum yinsh

#endif
