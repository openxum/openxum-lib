/*
 * @file tzaar/exception.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TZAAR_EXCEPTION_HPP
#define _TZAAR_EXCEPTION_HPP 1

#include <common/exception.hpp>

namespace openxum { namespace tzaar {

class InvalidColor : public std::runtime_error
{
public:
    InvalidColor(const std::string& argv = std::string()) :
        std::runtime_error(argv)
    { }
};

class InvalidType : public std::runtime_error
{
public:
    InvalidType(const std::string& argv = std::string()) :
        std::runtime_error(argv)
    { }
};

class InvalidPutting : public std::runtime_error
{
public:
    InvalidPutting(const std::string& argv = std::string()) :
        std::runtime_error(argv)
    { }
};

}} // namespace openxum tzaar

#endif
