/*
 * @file tzaar/board.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TZAAR_HPP
#define _TZAAR_HPP 1

#include <map>
#include <vector>

#include <tzaar/coordinates.hpp>
#include <tzaar/exception.hpp>
#include <tzaar/intersection.hpp>

namespace openxum { namespace tzaar {

enum GameType { REGULAR, BLITZ };

class Board
{
public:
    typedef std::map < openxum::tzaar::Coordinates,
                       Intersection > Intersections;

    enum Phase { FIRST_MOVE, CAPTURE, CHOOSE, SECOND_CAPTURE, MAKE_STRONGER,
                 PASS };

    Board(GameType type = REGULAR);
    Board(GameType type, common::Color color);

    virtual ~Board()
    { }

    bool can_capture(common::Color color) const;
    bool can_make_stack(common::Color color) const;
    void capture(const Coordinates& origin, const Coordinates& destination);
    void choose(Phase choice)
    { mPhase = choice; }
    common::Color current_color() const
    { return mCurrentColor; }
    bool exist_intersection(char letter, int number) const;
    void first_move(const Coordinates& origin, const Coordinates& destination);
    void get_piece_number(common::Color color, int& tzaarNumber,
                          int& tzarraNumber, int& tottNumber) const;
    CoordinatesList get_possible_capture(const Coordinates& coordinates) const;
    CoordinatesList get_possible_make_stack(
        const Coordinates& coordinates) const;
    common::Color intersection_color(char letter, int number) const
    { return get_intersection(letter, number)->color(); }
    State intersection_state(char letter, int number) const
    { return get_intersection(letter, number)->state(); }
    const Intersections& intersections() const
    { return mIntersections; }
    bool is_all_types_presented(common::Color color) const;
    bool is_finished() const;
    void make_stack(const Coordinates& origin, const Coordinates& destination);
    void no_capture();
    void pass();
    Phase phase() const
    { return mPhase; }
    std::string to_string() const;
    bool verify_capture(const Coordinates& origin,
                        const Coordinates& destination) const;
    common::Color winner_is() const;

    // STRATEGY
    CoordinatesList get_distant_neighbors(const Coordinates& origin,
                                          common::Color color,
                                          bool control = true) const;
    CoordinatesList get_no_free_intersections(common::Color color) const;
    CoordinatesList get_neighbors(const Coordinates& origin,
                                  common::Color color) const;

    // Constantes
    static const int begin_diagonal_number[];
    static const int end_diagonal_number[];
    static const char begin_diagonal_letter[];
    static const char end_diagonal_letter[];
    static const int begin_number[];
    static const int end_number[];
    static const char begin_letter[];
    static const char end_letter[];
    static const PieceType initial_type[];
    static const common::Color initial_color[];

private:
    void change_color();
    const Intersection* get_intersection(char letter, int number) const;
    Direction next_direction(Direction direction) const;

    Intersections mIntersections;
    GameType mType;
    common::Color mCurrentColor;
    Phase mPhase;
};

}} // namespace openxum tzaar

#endif
