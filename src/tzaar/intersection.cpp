/*
 * @file tzaar/intersection.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <tzaar/exception.hpp>
#include <tzaar/intersection.hpp>
#include <sstream>

#include <iostream>

using namespace openxum::common;

namespace openxum { namespace tzaar {

common::Color Intersection::color() const
{
    if (mState == VACANT) {
        throw InvalidColor("tzaar: no color");
    }
    return mStack.color();
}

void Intersection::capture(Intersection& destination)
{
    Stack stack;

    while (not mStack.empty()) {
        stack.put(mStack.remove_top());
    }
    mState = VACANT;
    destination.remove_stack();
    while (not stack.empty()) {
        Piece piece = stack.remove_top();

        destination.put_piece(piece.type(), piece.color());
    }
}

void Intersection::move_stack_to(Intersection& destination)
{
    Stack stack;

    while (not mStack.empty()) {
        stack.put(mStack.remove_top());
    }
    mState = VACANT;
    while (not stack.empty()) {
        Piece piece = stack.remove_top();

        destination.put_piece(piece.type(), piece.color());
    }
}

State Intersection::state() const
{
    return mState;
}

std::string Intersection::to_string() const
{
    std::ostringstream str;

    str << "[";
    if (mState == VACANT) {
        str << "   ";
    } else {
        if (mStack.color() == BLACK) {
            str << "B";
        } else {
            str << "W";
        }

        if (mStack.type() == TZAAR) {
            str << "Z";
        } else if (mStack.type() == TZARRA) {
            str << "z";
        } else if (mStack.type() == TOTT) {
            str << "T";
        }

        if (mStack.size() > 1) {
            str << mStack.size();
        } else {
            str << " ";
        }
    }
    str << "]";
    return str.str();
}

PieceType Intersection::type() const
{
    if (mState == VACANT) {
        throw InvalidType("tzaar: no type");
    }
    return mStack.type();
}

}} // namespace openxum tzaar
