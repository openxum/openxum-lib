/*
 * @file tzaar/intersection.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TZAAR_INTERSECTION_HPP
#define _TZAAR_INTERSECTION_HPP 1

#include <common/color.hpp>
#include <tzaar/coordinates.hpp>
#include <stack>
#include <string>

namespace openxum { namespace tzaar {

enum State { VACANT, NO_VACANT };
enum PieceType { TZAAR, TZARRA, TOTT };

class Piece
{
public:
    Piece(PieceType type, common::Color color) :  mType(type), mColor(color)
    { }

    Piece(const Piece& piece) : mType(piece.mType), mColor(piece.mColor)
    { }

    common::Color color() const
    { return mColor; }

    PieceType type() const
    { return mType; }

private:
    common::Color mColor;
    PieceType mType;
};

class Stack : private std::stack < Piece >
{
public:
    Stack()
    { }

    common::Color color() const
    { return top().color(); }

    void clear()
    {
        while (not empty()) {
            pop();
        }
    }

    bool empty() const
    { return std::stack < Piece >::empty(); }

    void put(Piece piece)
    { push(piece); }

    Piece remove_top()
    {
        Piece top = std::stack < Piece >::top();

        pop();
        return top;
    }

    int size() const
    { return std::stack < Piece >::size(); }

    PieceType type() const
    { return top().type(); }

};

class Intersection
{
public:
    Intersection(const openxum::tzaar::Coordinates& coordinates) :
        mCoordinates(coordinates), mState(VACANT)
    { }

    Intersection(const openxum::tzaar::Coordinates& coordinates,
                 PieceType type, common::Color color) :
        mCoordinates(coordinates), mState(NO_VACANT)
    { mStack.put(Piece(type, color)); }

    virtual ~Intersection()
    { }

    void capture(Intersection& destination);

    common::Color color() const;

    const openxum::tzaar::Coordinates& coordinates() const
    { return mCoordinates; }

    int height() const
    { return mStack.size(); }

    char letter() const
    { return mCoordinates.letter(); }

    void move_stack_to(Intersection& destination);

    int number() const
    { return mCoordinates.number(); }

    virtual bool operator==(const Intersection& intersection) const
    { mCoordinates == intersection.mCoordinates; }

    void put_piece(PieceType type, common::Color color)
    {
        mState = NO_VACANT;
        mStack.put(Piece(type, color));
    }

    void remove_stack()
    { mStack.clear(); }

    State state() const;
    std::string to_string() const;

    PieceType type() const;

private:
    openxum::tzaar::Coordinates mCoordinates;
    State mState;
    Stack mStack;
};

}} // namespace openxum tzaar

#endif
