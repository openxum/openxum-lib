/*
 * @file tzaar/coordinates.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TZAAR_COORDINATES_HPP
#define _TZAAR_COORDINATES_HPP 1

#include <common/coordinates.hpp>

namespace openxum { namespace tzaar {

enum Direction { NORTH_WEST, NORTH, NORTH_EAST, SOUTH_EAST, SOUTH, SOUTH_WEST };

class Coordinates : public openxum::common::Coordinates
{
public:
    Coordinates(char letter = 'A', int number = 1) :
        openxum::common::Coordinates(letter, number)
    { }

    virtual ~Coordinates()
    { }

    virtual void decode(const char* msg)
    {
        mLetter = msg[0];
        mNumber = msg[1] - '0';
    }

    virtual void encode(char* msg) const
    {
        msg[0] = mLetter;
        msg[1] = (char)('0'+ mNumber);
        msg[2] = 0;
    }

    virtual int key() const
    { return (mLetter - 'A') + (mNumber - 1) * 9; }

    virtual bool is_valid() const
    {
        return (mLetter == 'A' and mNumber >= 1 and mNumber <= 5) or
            (mLetter == 'B' and mNumber >= 1 and mNumber <= 6) or
            (mLetter == 'C' and mNumber >= 1 and mNumber <= 7) or
            (mLetter == 'D' and mNumber >= 1 and mNumber <= 8) or
            (mLetter == 'E' and mNumber >= 1 and mNumber <= 9) or
            (mLetter == 'F' and mNumber >= 2 and mNumber <= 9) or
            (mLetter == 'G' and mNumber >= 3 and mNumber <= 9) or
            (mLetter == 'H' and mNumber >= 4 and mNumber <= 9) or
            (mLetter == 'I' and mNumber >= 5 and mNumber <= 9);
    }

    Coordinates move(int distance, Direction direction) const;
};

typedef std::vector < Coordinates > CoordinatesList;

}} // namespace openxum tzaar

#endif
