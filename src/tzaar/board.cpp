/*
 * @file tzaar/board.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <tzaar/board.hpp>
#include <algorithm>

#include <iostream>

using namespace openxum::common;

namespace openxum { namespace tzaar {

const int Board::begin_diagonal_number[] = { 5, 4, 3, 2, 1, 1, 1, 1, 1 };
const int Board::end_diagonal_number[] = { 9, 9, 9, 9, 9, 8, 7, 6, 5 };
const char Board::begin_diagonal_letter[] = { 'A', 'A', 'A', 'A', 'A', 'B',
                                              'C', 'D', 'E' };
const char Board::end_diagonal_letter[] = { 'E', 'F', 'G', 'H', 'I', 'I',
                                            'I', 'I', 'I' };
const char Board::begin_letter[] = { 'A', 'A', 'A', 'A', 'A', 'B', 'C', 'D',
                                     'E' };
const char Board::end_letter[] = { 'E', 'F', 'G', 'H', 'I', 'I', 'I', 'I',
                                   'I' };
const int Board::begin_number[] = { 1, 1, 1, 1, 1, 2, 3, 4, 5 };
const int Board::end_number[] = { 5, 6, 7, 8, 9, 9, 9, 9, 9 };
const PieceType Board::initial_type[] = {
    // column A
    TOTT, TOTT, TOTT, TOTT, TOTT,
    // column B
    TOTT, TZARRA, TZARRA, TZARRA, TZARRA, TOTT,
    // column C
    TOTT, TZARRA, TZAAR, TZAAR, TZAAR, TZARRA, TOTT,
    // column D
    TOTT, TZARRA, TZAAR, TOTT, TOTT, TZAAR, TZARRA, TOTT,
    // column E
    TOTT, TZARRA, TZAAR, TOTT, TOTT, TZAAR, TZARRA, TOTT,
    // column F
    TOTT, TZARRA, TZAAR, TOTT, TOTT, TZAAR, TZARRA, TOTT,
    // column G
    TOTT, TZARRA, TZAAR, TZAAR, TZAAR, TZARRA, TOTT,
    // column H
    TOTT, TZARRA, TZARRA, TZARRA, TZARRA, TOTT,
    // column I
    TOTT, TOTT, TOTT, TOTT, TOTT
};

const common::Color Board::initial_color[] = {
    // column A
    BLACK, BLACK, BLACK, BLACK, WHITE,
    // column B
    WHITE, BLACK, BLACK, BLACK, WHITE, WHITE,
    // column C
    WHITE, WHITE, BLACK, BLACK, WHITE, WHITE, WHITE,
    // column D
    WHITE, WHITE, WHITE, BLACK, WHITE, WHITE, WHITE, WHITE,
    // column E
    WHITE, WHITE, WHITE, WHITE, BLACK, BLACK, BLACK, BLACK,
    // column F
    BLACK, BLACK, BLACK, BLACK, WHITE, BLACK, BLACK, BLACK,
    // column G
    BLACK, BLACK, BLACK, WHITE, WHITE, BLACK, BLACK,
    // column H
    BLACK, BLACK, WHITE, WHITE, WHITE, BLACK,
    // column I
    BLACK, WHITE, WHITE, WHITE, WHITE
};

Board::Board(GameType type) : mType(type), mPhase(FIRST_MOVE)
{
    int k = 0;

    for (char l = 'A'; l < 'J'; ++l) {
        for (int n = begin_number[l - 'A']; n <= end_number[l - 'A']; ++n) {
            if (l != 'E' and n != 5) {
                openxum::tzaar::Coordinates coordinates(l, n);

                mIntersections.insert(
                    std::make_pair(coordinates,
                                   Intersection(coordinates,
                                                initial_type[k],
                                                initial_color[k])));
                ++k;
            }
        }
    }
    mCurrentColor = (rand() % 1024 < 512) ? BLACK : WHITE;
}

Board::Board(GameType type, Color color) : mType(type),
                                           mCurrentColor(color),
                                           mPhase(FIRST_MOVE)
{
    int k = 0;

    for (char l = 'A'; l < 'J'; ++l) {
        for (int n = begin_number[l - 'A']; n <= end_number[l - 'A']; ++n) {
            if (l != 'E' or n != 5) {
                openxum::tzaar::Coordinates coordinates(l, n);

                mIntersections.insert(
                    std::make_pair(coordinates,
                                   Intersection(coordinates,
                                                initial_type[k],
                                                initial_color[k])));
                ++k;
            }
        }
    }
}

bool Board::can_capture(Color color) const
{
    bool found = false;
    CoordinatesList list = get_no_free_intersections(color);
    int index = 0;

    while (not found and index < list.size()) {
        found = not get_distant_neighbors(
            list[index], color == BLACK ? WHITE : BLACK).empty();
        ++index;
    }
    return found;
}

bool Board::can_make_stack(Color color) const
{
    bool found = false;
    CoordinatesList list = get_no_free_intersections(color);
    int index = 0;

    while (not found and index < list.size()) {
        found = not get_distant_neighbors(list[index], color).empty();
        ++index;
    }
    return found;
}

void Board::capture(const Coordinates& origin,
                    const Coordinates& destination)
{
    Intersections::iterator origin_it = mIntersections.find(origin);
    Intersections::iterator destination_it = mIntersections.find(destination);

    if (mPhase == FIRST_MOVE and origin_it->second.color() != WHITE) {
        throw InvalidColor("tzaar: white player begins");
    }

    origin_it->second.capture(destination_it->second);
    if (mPhase == CAPTURE) {
        mPhase = CHOOSE;
    } else if (mPhase == SECOND_CAPTURE) {
        mPhase = CAPTURE;
        change_color();
    }
}

void Board::change_color()
{
    if (mCurrentColor == WHITE) {
        mCurrentColor = BLACK;
    } else {
        mCurrentColor = WHITE;
    }
}

bool Board::exist_intersection(char letter, int number) const
{
    openxum::tzaar::Coordinates coordinates(letter, number);

    if (coordinates.is_valid()) {
        Intersections::const_iterator it = mIntersections.find(coordinates);

        return it != mIntersections.end();
    } else {
        return false;
    }
}

void Board::first_move(const Coordinates& origin,
                       const Coordinates& destination)
{
    Intersections::iterator origin_it = mIntersections.find(origin);
    Intersections::iterator destination_it = mIntersections.find(destination);

    if (destination_it->second.color() == WHITE) {
        throw InvalidColor("tzaar: white player begins");
    }

    origin_it->second.capture(destination_it->second);
    mPhase = CAPTURE;
    change_color();
}

CoordinatesList Board::get_distant_neighbors(const Coordinates& origin,
                                             common::Color color,
                                             bool control) const
{
    CoordinatesList list;
    Intersections::const_iterator origin_it = mIntersections.find(origin);
    Direction direction = NORTH_WEST;
    bool stop = false;

    while (not stop) {
        int distance = 0;
        Coordinates destination;
        Intersections::const_iterator destination_it;

        do {
            ++distance;
            destination = origin.move(distance, direction);
            destination_it = mIntersections.find(destination);
        } while (destination.is_valid() and
                 destination_it->second.state() == VACANT);
        if (destination.is_valid() and
            destination_it->second.state() == NO_VACANT and
            destination_it->second.color() == color) {
            if (not control or
                (control and
                 origin_it->second.height() >= destination_it->second.height()))
            {
                list.push_back(destination);
            }
        }
        if (direction == SOUTH_WEST) {
            stop = true;
        } else {
            direction = next_direction(direction);
        }
    }
    return list;
}

const Intersection* Board::get_intersection(char letter, int number) const
{
    openxum::tzaar::Coordinates coordinates(letter, number);
    Intersections::const_iterator it = mIntersections.find(coordinates);

    if (it != mIntersections.end()) {
        return &(it->second);
    } else {
        return 0;
    }
}

CoordinatesList Board::get_neighbors(const Coordinates& origin,
                                     common::Color color) const
{
    CoordinatesList list;
    Intersections::const_iterator it = mIntersections.find(origin);
    Direction direction = NORTH_WEST;
    bool stop = false;

    while (not stop) {
        Coordinates destination = origin.move(1, direction);
        Intersections::const_iterator destination_it =
            mIntersections.find(destination);

        if (destination.is_valid() and
            destination_it->second.state() == NO_VACANT and
            destination_it->second.color() == color) {
            list.push_back(destination);
        }
        if (direction == SOUTH_WEST) {
            stop = true;
        } else {
            direction = next_direction(direction);
        }
    }
    return list;
}

CoordinatesList Board::get_no_free_intersections(common::Color color) const
{
    CoordinatesList list;
    Intersections::const_iterator it = mIntersections.begin();

    while (it != mIntersections.end()) {
        if (it->second.state() == NO_VACANT and it->second.color() == color) {
            list.push_back(it->first);
        }
        ++it;
    }
    return list;
}

void Board::get_piece_number(common::Color color, int& tzaarNumber,
                             int& tzarraNumber, int& tottNumber) const
{
    Intersections::const_iterator it = mIntersections.begin();

    tzaarNumber = tzarraNumber = tottNumber = 0;
    while (it != mIntersections.end()) {
        if (it->second.state() == NO_VACANT and it->second.color() == color) {
            if (it->second.type() == TZAAR) {
                ++tzaarNumber;
            }
            if (it->second.type() == TZARRA) {
                ++tzarraNumber;
            }
            if (it->second.type() == TOTT) {
                ++tottNumber;
            }
        }
        ++it;
    }
}

CoordinatesList Board::get_possible_capture(
    const Coordinates& coordinates) const
{
    Intersections::const_iterator it = mIntersections.find(coordinates);

    return get_distant_neighbors(
        coordinates, it->second.color() == BLACK ? WHITE : BLACK);
}

CoordinatesList Board::get_possible_make_stack(
    const Coordinates& coordinates) const
{
    Intersections::const_iterator it = mIntersections.find(coordinates);

    return get_distant_neighbors(coordinates, it->second.color(), false);
}

bool Board::is_finished() const
{
    return (not can_capture(BLACK) or
            not is_all_types_presented(BLACK) or
            not can_capture(WHITE) or
            not is_all_types_presented(WHITE));
}

bool Board::is_all_types_presented(common::Color color) const
{
    bool tzaar_found = false;
    bool tzara_found = false;
    bool tott_found = false;

    Intersections::const_iterator it = mIntersections.begin();

    while (it != mIntersections.end() and not (tzaar_found and tzara_found
                                               and tott_found)) {
        if (it->second.state() == NO_VACANT and it->second.color() == color) {
            if (it->second.type() == TZAAR) {
                tzaar_found = true;
            }
            if (it->second.type() == TZARRA) {
                tzara_found = true;
            }
            if (it->second.type() == TOTT) {
                tott_found = true;
            }
        }
        ++it;
    }
    return tzaar_found and tzara_found and tott_found;
}

void Board::make_stack(const Coordinates& origin,
                       const Coordinates& destination)
{
    Intersections::iterator origin_it = mIntersections.find(origin);
    Intersections::iterator destination_it = mIntersections.find(destination);

    if (origin_it->second.color() != destination_it->second.color()) {
        throw InvalidColor("tzaar: invalid color while a stack makes");
    }

    origin_it->second.move_stack_to(destination_it->second);
    mPhase = CAPTURE;
    change_color();
}

Direction Board::next_direction(Direction direction) const
{
    if (direction == NORTH_WEST) {
        return NORTH;
    } else if (direction == NORTH) {
        return NORTH_EAST;
    } else if (direction == NORTH_EAST) {
        return SOUTH_EAST;
    } else if (direction == SOUTH_EAST) {
        return SOUTH;
    } else if (direction == SOUTH) {
        return SOUTH_WEST;
    } else if (direction == SOUTH_WEST) {
        return NORTH_WEST;
    }
}

void Board::no_capture()
{
}

void Board::pass()
{
    mPhase = CAPTURE;
    change_color();
}

const char letter_matrix[] = {
    'X', 'X', 'X', 'X', 'Z', 'X', 'X', 'X', 'X', 'X',
    'X', 'X', 'X', 'Z', 'X', 'E', 'X', 'X', 'X', 'X',
    'X', 'X', 'Z', 'X', 'D', 'X', 'F', 'X', 'X', 'X',
    'X', 'Z', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'X',
    'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X',
    'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I',
    'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X',
    'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I',
    'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X',
    'X', 'A', 'X', 'C', 'X', 'X', 'X', 'G', 'X', 'I',
    'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X',
    'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I',
    'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X',
    'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I',
    'X', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X',
    'X', 'X', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'X',
    'X', 'X', 'X', 'X', 'D', 'X', 'F', 'X', 'X', 'X',
    'X', 'X', 'X', 'X', 'X', 'E', 'X', 'X', 'X', 'X'
};

const char number_matrix[] = {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 9, 0, 0, 0, 0,
    0, 0, 0, 0, 8, 0, 9, 0, 0, 0,
    0, 0, 0, 7, 0, 8, 0, 9, 0, 0,
    0, 0, 6, 0, 7, 0, 8, 0, 9, 0,
    0, 5, 0, 6, 0, 7, 0, 8, 0, 9,
    0, 0, 5, 0, 6, 0, 7, 0, 8, 0,
    0, 4, 0, 5, 0, 6, 0, 7, 0, 8,
    0, 0, 4, 0, 5, 0, 6, 0, 7, 0,
    0, 3, 0, 4, 0, 0, 0, 6, 0, 7,
    0, 0, 3, 0, 4, 0, 5, 0, 6, 0,
    0, 2, 0, 3, 0, 4, 0, 5, 0, 6,
    0, 0, 2, 0, 3, 0, 4, 0, 5, 0,
    0, 1, 0, 2, 0, 3, 0, 4, 0, 5,
    0, 0, 1, 0, 2, 0, 3, 0, 4, 0,
    0, 0, 0, 1, 0, 2, 0, 3, 0, 0,
    0, 0, 0, 0, 1, 0, 2, 0, 0, 0,
    0, 0, 0, 0, 0, 1, 0, 0, 0, 0
};

std::string Board::to_string() const
{
    std::string str;
    unsigned int k = 9;

    for (int j = 0; j < 18; ++j) {
        for (int i = 0; i < 10; ++i) {
            char l = letter_matrix[i + 10 * j];
            int n = number_matrix[i + 10 * j];

            if (l != 'X' and l != 'Z') {
                openxum::tzaar::Coordinates coordinates(l, n);
                Intersections::const_iterator it =
                    mIntersections.find(coordinates);

                if (it != mIntersections.end()) {
                    str += it->second.to_string();
                }
            } else if (l == 'Z') {
                std::string s("    ");

                s += '0' + k;
                str += s;
                --k;
            } else {
                str += "     ";
            }
        }
        str += "\n";
    }
    str += "      A    B    C    D    E    F    G    H    I\n";
    return str;
}

bool Board::verify_capture(const Coordinates& origin,
                           const Coordinates& destination) const
{
    Intersections::const_iterator origin_it = mIntersections.find(origin);
    CoordinatesList list = get_distant_neighbors(
        origin, origin_it->second.color() == BLACK ? WHITE : BLACK);

    return std::find(list.begin(), list.end(), destination) != list.end();
}

Color Board::winner_is() const
{
    if (is_finished()) {
        if (not can_capture(BLACK) or
            not is_all_types_presented(BLACK)) {
            return WHITE;
        } else {
            return BLACK;
        }
    } else {
        throw InvalidWinner("tzaar: game is not finished");
    }
}

}} // namespace openxum tzaar
