/*
 * @file zertz/intersection.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <zertz/exception.hpp>
#include <zertz/intersection.hpp>

using namespace openxum::common;

namespace openxum { namespace zertz {

Color Intersection::color() const
{
    if (mState == VACANT or mState == EMPTY) {
        throw InvalidColor("yinsh: no color");
    }

    if (mState == BLACK_MARBLE) {
        return BLACK;
    } else if (mState == GREY_MARBLE) {
        return GREY;
    } else {
        return WHITE;
    }
}

void Intersection::put_marble(Color color)
{
    if (mState != VACANT) {
        throw InvalidMoving("zertz: invalid intersection to marble putting");
    }

    if (color == BLACK) {
        mState = BLACK_MARBLE;
    } else if (color == WHITE) {
        mState = WHITE_MARBLE;
    } else if (color == GREY) {
        mState = GREY_MARBLE;
    }
}

void Intersection::remove_marble()
{
    if (mState != BLACK_MARBLE and mState != WHITE_MARBLE and
        mState != GREY_MARBLE) {
        throw InvalidMoving("zertz: invalid intersection to marble removing");
    }

    mState = VACANT;
}

void Intersection::remove_ring()
{
    if (mState == VACANT) {
        mState = EMPTY;
    } else {
        throw InvalidMoving("yinsh: invalid ring to removing");
    }
}

State Intersection::state() const
{
    return mState;
}

std::string Intersection::to_string() const
{
    switch (mState) {
    case VACANT: return "[V  ]";
    case BLACK_MARBLE: return "[BM ]";
    case WHITE_MARBLE: return "[WM ]";
    case GREY_MARBLE: return "[GM ]";
    default: return "[E  ]";
    }
}

}} // namespace openxum zertz
