/*
 * @file zertz/board.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ZERTZ_BOARD_HPP
#define _ZERTZ_BOARD_HPP 1

#include <map>
#include <vector>

#include <zertz/coordinates.hpp>
#include <zertz/exception.hpp>
#include <zertz/intersection.hpp>

namespace openxum { namespace zertz {

enum Type { REGULAR, BLITZ };
enum Number { ONE, TWO };

typedef std::vector < const Intersection* > PossibleMovingList;

class Board
{
public:
    typedef std::map < Coordinates, Intersection > Intersections;

    Board(Type type = REGULAR);
    Board(Type type, Number player);

    virtual ~Board()
    { }

    unsigned int available_black_marble_number() const
    { return mBlackMarbleNumber; }
    unsigned int available_grey_marble_number() const
    { return mGreyMarbleNumber; }
    unsigned int available_white_marble_number() const
    { return mWhiteMarbleNumber; }
    bool can_capture() const;
    void capture(const Coordinates& origin,
                 const Coordinates& destination,
                 const CoordinatesList& captured,
                 Number player);
    void capture_marble_and_ring(const CoordinatesList& captured,
                                 Number player);
    unsigned int captured_marble_number(common::Color color,
                                        Number number) const;
    Number current_player() const
    { return mCurrentPlayer; }
    bool exist_intersection(char letter, int number) const;
    State intersection_state(char letter, int number) const
    { return get_intersection(letter, number)->state(); }
    common::Color intersection_color(char letter, int number) const
    { return get_intersection(letter, number)->color(); }
    const Intersections& intersections() const
    { return mIntersections; }
    bool is_finished() const;
    Coordinates jump_marble(const Coordinates& origin,
                            const Coordinates& captured,
                            Number player);
    void put_marble(const Coordinates& coordinates, common::Color color,
                    Number player);
    void remove_ring(const Coordinates& coordinates, Number player);
    std::string to_string() const;
    Number winner_is() const;

    // strategy
    CoordinatesList get_free_rings() const;
    CoordinatesList get_can_capture_marbles() const;
    CoordinatesList get_isolated_marbles() const;
    CoordinatesList get_possible_capturing_marbles(
        const Coordinates& coordinates) const;
    CoordinatesList get_possible_removing_rings() const;
    bool is_possible_capturing_marbles(const Coordinates& coordinates) const;
    bool is_possible_to_capture_with(const Intersection& intersection) const;

    // Constantes
    static const char begin_letter[];
    static const char end_letter[];
    static const int begin_number[];
    static const int end_number[];
    static const char begin_diagonal_letter[];
    static const char end_diagonal_letter[];
    static const int begin_diagonal_number[];
    static const int end_diagonal_number[];

private:
    void capture(Intersection& intersection, Number player);
    void change_player();
    const Intersection* get_intersection(char letter, int number) const;
    bool is_finished(Number player) const;
    bool is_isolated_marble(const Intersection* intersection) const;

    Intersections mIntersections;
    Type mType;

    Number mCurrentPlayer;

    unsigned int mBlackMarbleNumber;
    unsigned int mGreyMarbleNumber;
    unsigned int mWhiteMarbleNumber;

    unsigned int mCapturedBlackMarbleNumberByOne;
    unsigned int mCapturedGreyMarbleNumberByOne;
    unsigned int mCapturedWhiteMarbleNumberByOne;

    unsigned int mCapturedBlackMarbleNumberByTwo;
    unsigned int mCapturedGreyMarbleNumberByTwo;
    unsigned int mCapturedWhiteMarbleNumberByTwo;
};

}} // namespace openxum zertz

#endif
