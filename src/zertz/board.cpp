/*
 * @file zertz/board.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <stdlib.h>
#include <stack>
#include <vector>
#include <zertz/board.hpp>

using namespace openxum::common;

namespace openxum { namespace zertz {

const int Board::begin_number[] = { 1, 1, 1, 1, 2, 3, 4 };
const int Board::end_number[]   = { 4, 5, 6, 7, 7, 7, 7 };

const unsigned int initial_black_marble_number = 10;
const unsigned int initial_grey_marble_number = 8;
const unsigned int initial_white_marble_number = 6;

Board::Board(Type type) : mType(type),
                          mBlackMarbleNumber(initial_black_marble_number),
                          mGreyMarbleNumber(initial_grey_marble_number),
                          mWhiteMarbleNumber(initial_white_marble_number),
                          mCapturedBlackMarbleNumberByOne(0),
                          mCapturedGreyMarbleNumberByOne(0),
                          mCapturedWhiteMarbleNumberByOne(0),
                          mCapturedBlackMarbleNumberByTwo(0),
                          mCapturedGreyMarbleNumberByTwo(0),
                          mCapturedWhiteMarbleNumberByTwo(0)
{
    if (type == BLITZ) {
        --mBlackMarbleNumber;
        --mGreyMarbleNumber;
        --mWhiteMarbleNumber;
    }

    for (char l = 'A'; l < 'H'; ++l) {
        for (int n = begin_number[l - 'A']; n <= end_number[l - 'A']; ++n) {
            Coordinates coordinates(l, n);

            mIntersections.insert(
                std::make_pair(coordinates, Intersection(coordinates)));
        }
    }
    mCurrentPlayer = (rand() % 1024 < 512) ? ONE : TWO;
}

Board::Board(Type type,
             Number player) : mType(type),
                              mBlackMarbleNumber(initial_black_marble_number),
                              mGreyMarbleNumber(initial_grey_marble_number),
                              mWhiteMarbleNumber(initial_white_marble_number),
                              mCapturedBlackMarbleNumberByOne(0),
                              mCapturedGreyMarbleNumberByOne(0),
                              mCapturedWhiteMarbleNumberByOne(0),
                              mCapturedBlackMarbleNumberByTwo(0),
                              mCapturedGreyMarbleNumberByTwo(0),
                              mCapturedWhiteMarbleNumberByTwo(0),
                              mCurrentPlayer(player)
{
    for (char l = 'A'; l < 'H'; ++l) {
        for (int n = begin_number[l - 'A']; n <= end_number[l - 'A']; ++n) {
            Coordinates coordinates(l, n);

            mIntersections.insert(
                std::make_pair(coordinates, Intersection(coordinates)));
        }
    }
}

bool Board::can_capture() const
{
    return not get_can_capture_marbles().empty();
}

void Board::capture(Intersection& intersection, Number player)
{
    if (intersection.state() == BLACK_MARBLE) {
        if (player == ONE) {
            ++mCapturedBlackMarbleNumberByOne;
        } else {
            ++mCapturedBlackMarbleNumberByTwo;
        }
    } else if (intersection.state() == GREY_MARBLE) {
        if (player == ONE) {
            ++mCapturedGreyMarbleNumberByOne;
        } else {
            ++mCapturedGreyMarbleNumberByTwo;
        }
    } else {
        if (player == ONE) {
            ++mCapturedWhiteMarbleNumberByOne;
        } else {
            ++mCapturedWhiteMarbleNumberByTwo;
        }
    }
    intersection.remove_marble();
}

void Board::capture(const Coordinates& origin,
                    const Coordinates& destination,
                    const CoordinatesList& captured,
                    Number player)
{
    Intersections::iterator ito = mIntersections.find(origin);
    Intersections::iterator itd = mIntersections.find(destination);
    Color color = ito->second.color();

    ito->second.remove_marble();
    {
        CoordinatesList::const_iterator it = captured.begin();

        while (it != captured.end()) {
            Intersections::iterator itc = mIntersections.find(*it);

            capture(itc->second, player);
            ++it;
        }
    }
    itd->second.put_marble(color);
    if (not is_possible_to_capture_with(destination)) {
        change_player();
    }
}

void Board::capture_marble_and_ring(const CoordinatesList& captured,
                                    Number player)
{
    CoordinatesList::const_iterator it = captured.begin();

    while (it != captured.end()) {
        Intersections::iterator itc = mIntersections.find(*it);

        capture(itc->second, player);
        itc->second.remove_ring();
        ++it;
    }
    change_player();
}

unsigned int Board::captured_marble_number(Color color, Number number) const
{
    if (number == ONE) {
        if (color == BLACK) {
            return mCapturedBlackMarbleNumberByOne;
        } else if (color == GREY) {
            return mCapturedGreyMarbleNumberByOne;
        } else {
            return mCapturedWhiteMarbleNumberByOne;
        }
    } else {
        if (color == BLACK) {
            return mCapturedBlackMarbleNumberByTwo;
        } else if (color == GREY) {
            return mCapturedGreyMarbleNumberByTwo;
        } else {
            return mCapturedWhiteMarbleNumberByTwo;
        }
    }
}

void Board::change_player()
{
    if (mCurrentPlayer == TWO) {
        mCurrentPlayer = ONE;
    } else {
        mCurrentPlayer = TWO;
    }
}

bool Board::exist_intersection(char letter, int number) const
{
    Coordinates coordinates(letter, number);

    if (coordinates.is_valid()) {
        Intersections::const_iterator it = mIntersections.find(coordinates);

        return it != mIntersections.end();
    } else {
        return false;
    }
}

const Intersection* Board::get_intersection(char letter, int number) const
{
    Coordinates coordinates(letter, number);
    Intersections::const_iterator it = mIntersections.find(coordinates);

    if (it != mIntersections.end()) {
        return &(it->second);
    } else {
        return 0;
    }
}

bool Board::is_finished() const
{
    return is_finished(ONE) or is_finished(TWO);
}

bool Board::is_finished(Number player) const
{
    if (mType == BLITZ) {
        if (player == ONE) {
            return (mCapturedBlackMarbleNumberByOne == 2 and
                    mCapturedGreyMarbleNumberByOne == 2 and
                    mCapturedWhiteMarbleNumberByOne == 2) or
                (mCapturedBlackMarbleNumberByOne == 5 or
                 mCapturedGreyMarbleNumberByOne == 4 or
                 mCapturedWhiteMarbleNumberByOne == 3);
        } else {
            return (mCapturedBlackMarbleNumberByTwo == 2 and
                    mCapturedGreyMarbleNumberByTwo == 2 and
                    mCapturedWhiteMarbleNumberByTwo == 2) or
                (mCapturedBlackMarbleNumberByTwo == 5 or
                 mCapturedGreyMarbleNumberByTwo == 4 or
                 mCapturedWhiteMarbleNumberByTwo == 3);
        }
    } else { // type = REGULAR
        if (player == ONE) {
            return (mCapturedBlackMarbleNumberByOne == 3 and
                    mCapturedGreyMarbleNumberByOne == 3 and
                    mCapturedWhiteMarbleNumberByOne == 3) or
                (mCapturedBlackMarbleNumberByOne == 6 or
                 mCapturedGreyMarbleNumberByOne == 5 or
                 mCapturedWhiteMarbleNumberByOne == 4);
        } else {
            return (mCapturedBlackMarbleNumberByTwo == 3 and
                    mCapturedGreyMarbleNumberByTwo == 3 and
                    mCapturedWhiteMarbleNumberByTwo == 3) or
                (mCapturedBlackMarbleNumberByTwo == 6 or
                 mCapturedGreyMarbleNumberByTwo == 5 or
                 mCapturedWhiteMarbleNumberByTwo == 4);
        }
    }
}

bool Board::is_isolated_marble(const Intersection* intersection) const
{
    if (intersection->marble_is_present()) {
        std::stack < const Intersection* > list;
        std::vector < const Intersection* > visited;
        bool stop = false;

        list.push(intersection);
        while (not list.empty() and not stop) {
            const Intersection* current = list.top();
            char letter = current->letter();
            int number = current->number();
            const Intersection* N = get_intersection(letter, number + 1);
            const Intersection* NE = get_intersection(letter + 1, number + 1);
            const Intersection* SE = get_intersection(letter + 1, number);
            const Intersection* S = get_intersection(letter, number - 1);
            const Intersection* SO = get_intersection(letter - 1, number - 1);
            const Intersection* NO = get_intersection(letter - 1, number);

            visited.push_back(current);
            list.pop();
            if (N and N->state() == VACANT) {
                stop = true;
            } else if (N and N->state() != EMPTY) {
                if (std::find(visited.begin(), visited.end(), N) ==
                    visited.end()) {
                    list.push(N);
                }
            }
            if (NE and NE->state() == VACANT) {
                stop = true;
            } else if (NE and NE->state() != EMPTY) {
                if (std::find(visited.begin(), visited.end(), NE) ==
                    visited.end()) {
                    list.push(NE);
                }
            }
            if (SE and SE->state() == VACANT) {
                stop = true;
            } else if (SE and SE->state() != EMPTY) {
                if (std::find(visited.begin(), visited.end(), SE) ==
                    visited.end()) {
                    list.push(SE);
                }
            }
            if (S and S->state() == VACANT) {
                stop = true;
            } else if (S and S->state() != EMPTY) {
                if (std::find(visited.begin(), visited.end(), S) ==
                    visited.end()) {
                    list.push(S);
                }
            }
            if (SO and SO->state() == VACANT) {
                stop = true;
            } else if (SO and SO->state() != EMPTY) {
                if (std::find(visited.begin(), visited.end(), SO) ==
                    visited.end()) {
                    list.push(SO);
                }
            }
            if (NO and NO->state() == VACANT) {
                stop = true;
            } else if (NO and NO->state() != EMPTY) {
                if (std::find(visited.begin(), visited.end(), NO) ==
                    visited.end()) {
                    list.push(NO);
                }
            }
        }
        return not stop;
    } else {
        return false;
    }
}

Coordinates Board::jump_marble(const Coordinates& origin,
                               const Coordinates& captured,
                               Number player)
{
    Coordinates destination(
        origin.letter() + 2 * (captured.letter() - origin.letter()),
        origin.number() + 2 * (captured.number() - origin.number()));
    Intersections::iterator ito = mIntersections.find(origin);
    Intersections::iterator itc = mIntersections.find(captured);
    Intersections::iterator itd = mIntersections.find(destination);
    Color color = ito->second.color();

    ito->second.remove_marble();
    capture(itc->second, player);
    itd->second.put_marble(color);

    if (not is_possible_to_capture_with(itd->second)) {
        change_player();
    }

    return destination;
}

void Board::put_marble(const Coordinates& coordinates, Color color,
                       Number player)
{
    Intersections::iterator it = mIntersections.find(coordinates);

    if (it != mIntersections.end()) {
        it->second.put_marble(color);
        if (color == BLACK) {
            --mBlackMarbleNumber;
        } else if (color == GREY) {
            --mGreyMarbleNumber;
        } else {
            --mWhiteMarbleNumber;
        }
    }
}

void Board::remove_ring(const Coordinates& coordinates, Number player)
{
    Intersections::iterator it = mIntersections.find(coordinates);

    if (it != mIntersections.end()) {
        it->second.remove_ring();
    }
    if (get_isolated_marbles().empty()) {
        change_player();
    }
}

const char letter_matrix[] = {
    'X', 'X', 'Z', 'X', 'D', 'X', 'X', 'X',
    'X', 'Z', 'X', 'C', 'X', 'E', 'X', 'X',
    'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X',
    'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G',
    'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X',
    'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G',
    'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X',
    'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G',
    'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X',
    'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G',
    'X', 'X', 'B', 'X', 'D', 'X', 'F', 'X',
    'X', 'X', 'X', 'C', 'X', 'E', 'X', 'X',
    'X', 'X', 'X', 'X', 'D', 'X', 'X', 'X'
};

const char number_matrix[] = {
    0, 0, 0, 0, 7, 0, 0, 0,
    0, 0, 0, 6, 0, 7, 0, 0,
    0, 0, 5, 0, 6, 0, 7, 0,
    0, 4, 0, 5, 0, 6, 0, 7,
    0, 0, 4, 0, 5, 0, 6, 0,
    0, 3, 0, 4, 0, 5, 0, 6,
    0, 0, 3, 0, 4, 0, 5, 0,
    0, 2, 0, 3, 0, 4, 0, 5,
    0, 0, 2, 0, 3, 0, 4, 0,
    0, 1, 0, 2, 0, 3, 0, 4,
    0, 0, 1, 0, 2, 0, 3, 0,
    0, 0, 0, 1, 0, 2, 0, 0,
    0, 0, 0, 0, 1, 0, 0, 0
};

std::string Board::to_string() const
{
    std::string str("                   7\n");
    unsigned int k = 6;

    for (int j = 0; j < 13; ++j) {
        for (int i = 0; i < 8; ++i) {
            char l = letter_matrix[i + 8 * j];
            int n = number_matrix[i + 8 * j];

            if (l != 'X' and l != 'Z') {
                Coordinates coordinates(l, n);
                Intersections::const_iterator it =
                    mIntersections.find(coordinates);

                if (it != mIntersections.end()) {
                    str += it->second.to_string();
                }
            } else if (l == 'Z') {
                std::string s("    ");

                s += '0' + k;
                str += s;
                --k;
            } else {
                str += "     ";
            }
        }
        str += "\n";
    }
    str += "      A    B    C    D    E    F    G\n";
    str += " One : ";
    str += '0' + mCapturedBlackMarbleNumberByOne;
    str += " ";
    str += '0' + mCapturedGreyMarbleNumberByOne;
    str += " ";
    str += '0' + mCapturedWhiteMarbleNumberByOne;
    str += "\n";
    str += " Two : ";
    str += '0' + mCapturedBlackMarbleNumberByTwo;
    str += " ";
    str += '0' + mCapturedGreyMarbleNumberByTwo;
    str += " ";
    str += '0' + mCapturedWhiteMarbleNumberByTwo;
    str += "\n";
    return str;
}

Number Board::winner_is() const
{
    if (is_finished()) {
        if (is_finished(ONE)) {
            return ONE;
        } else {
            return TWO;
        }
    } else {
        throw InvalidWinner("yinsh: game is not finished");
    }
}

/*  - - - - - - - - - - - - - --ooOoo-- - - - - - - - - - - -  */

CoordinatesList Board::get_can_capture_marbles() const
{
    CoordinatesList list;
    Intersections::const_iterator it = mIntersections.begin();

    while (it != mIntersections.end()) {
        if (is_possible_to_capture_with(it->second)) {
            list.push_back(it->first);
        }
        ++it;
    }
    return list;
}

CoordinatesList Board::get_free_rings() const
{
    CoordinatesList list;
    Intersections::const_iterator it = mIntersections.begin();

    while (it != mIntersections.end()) {
        if (it->second.state() == VACANT) {
            list.push_back(it->first);
        }
        ++it;
    }
    return list;
}

CoordinatesList Board::get_isolated_marbles() const
{
    CoordinatesList list;
    Intersections::const_iterator it = mIntersections.begin();

    while (it != mIntersections.end()) {
        if (is_isolated_marble(&it->second)) {
            list.push_back(it->first);
        }
        ++it;
    }
    return list;
}

CoordinatesList Board::get_possible_capturing_marbles(
        const Coordinates& coordinates) const
{
    CoordinatesList list;
    Intersections::const_iterator it = mIntersections.find(coordinates);

    if (it->second.marble_is_present()) {
        char letter = it->second.letter();
        int number = it->second.number();

        // letter + number increase
        if (get_intersection(letter, number + 1) and
            get_intersection(letter, number + 2) and
            get_intersection(letter, number + 1)->marble_is_present() and
            get_intersection(letter, number + 2)->state() == VACANT) {
            list.push_back(Coordinates(letter, number + 1));
        }

        // letter + number decrease
        if (get_intersection(letter, number - 1) and
            get_intersection(letter, number - 2) and
            get_intersection(letter, number - 1)->marble_is_present() and
            get_intersection(letter, number - 2)->state() == VACANT) {
            list.push_back(Coordinates(letter, number - 1));
        }

        // letter increase + number
        if (get_intersection(letter + 1, number) and
            get_intersection(letter + 2, number) and
            get_intersection(letter + 1, number)->marble_is_present() and
            get_intersection(letter + 2, number)->state() == VACANT) {
            list.push_back(Coordinates(letter + 1, number));
        }

        // letter decrease + number
        if (get_intersection(letter - 1, number) and
            get_intersection(letter - 2, number) and
            get_intersection(letter - 1, number)->marble_is_present() and
            get_intersection(letter - 2, number)->state() == VACANT) {
            list.push_back(Coordinates(letter - 1, number));
        }

        // letter increase + number increase
        if (get_intersection(letter + 1, number + 1) and
            get_intersection(letter + 2, number + 2) and
            get_intersection(letter + 1,
                             number + 1)->marble_is_present() and
            get_intersection(letter + 2, number + 2)->state() == VACANT) {
            list.push_back(Coordinates(letter + 1, number + 1));
        }

        // letter decrease + number decrease
        if (get_intersection(letter - 1, number - 1) and
            get_intersection(letter - 2, number - 2) and
            get_intersection(letter - 1,
                             number - 1)->marble_is_present() and
            get_intersection(letter - 2, number - 2)->state() == VACANT) {
            list.push_back(Coordinates(letter - 1, number - 1));
        }
    }
    return list;
}

CoordinatesList Board::get_possible_removing_rings() const
{
    CoordinatesList list;
    Intersections::const_iterator it = mIntersections.begin();

    while (it != mIntersections.end()) {
        if (it->second.state() == VACANT) {
            char letter = it->second.letter();
            int number = it->second.number();
            const Intersection* neighbour = 0;

            // letter + number increase
            neighbour = get_intersection(letter, number + 1);
            if (not neighbour or (neighbour and neighbour->state() == EMPTY)) {
                const Intersection* next_neighbour =
                    get_intersection(letter + 1, number + 1);

                if (not next_neighbour or
                    (next_neighbour and next_neighbour->state() == EMPTY)) {
                    list.push_back(it->first);
                }
            }

            // letter + number decrease
            neighbour = get_intersection(letter, number - 1);
            if (not neighbour or (neighbour and neighbour->state() == EMPTY)) {
                const Intersection* next_neighbour =
                    get_intersection(letter - 1, number - 1);

                if (not next_neighbour or
                    (next_neighbour and next_neighbour->state() == EMPTY)) {
                    list.push_back(it->first);
                }
            }

            // letter increase + number
            neighbour = get_intersection(letter + 1, number);
            if (not neighbour or (neighbour and neighbour->state() == EMPTY)) {
                const Intersection* next_neighbour =
                    get_intersection(letter, number - 1);

                if (not next_neighbour or
                    (next_neighbour and next_neighbour->state() == EMPTY)) {
                    list.push_back(it->first);
                }
            }

            // letter decrease + number
            neighbour = get_intersection(letter - 1, number);
            if (not neighbour or (neighbour and neighbour->state() == EMPTY)) {
                const Intersection* next_neighbour =
                    get_intersection(letter, number + 1);

                if (not next_neighbour or
                    (next_neighbour and next_neighbour->state() == EMPTY)) {
                    list.push_back(it->first);
                }
            }

            // letter increase + number increase
            neighbour = get_intersection(letter + 1, number + 1);
            if (not neighbour or (neighbour and neighbour->state() == EMPTY)) {
                const Intersection* next_neighbour =
                    get_intersection(letter + 1, number);

                if (not next_neighbour or
                    (next_neighbour and next_neighbour->state() == EMPTY)) {
                    list.push_back(it->first);
                }
            }

            // letter decrease + number decrease
            neighbour = get_intersection(letter - 1, number - 1);
            if (not neighbour or (neighbour and neighbour->state() == EMPTY)) {
                const Intersection* next_neighbour =
                    get_intersection(letter - 1, number);

                if (not next_neighbour or
                    (next_neighbour and next_neighbour->state() == EMPTY)) {
                    list.push_back(it->first);
                }
            }
        }
        ++it;
    }
    return list;
}

bool Board::is_possible_capturing_marbles(const Coordinates& coordinates) const
{
    Intersections::const_iterator it = mIntersections.find(coordinates);

    return is_possible_to_capture_with(it->second);
}

bool Board::is_possible_to_capture_with(const Intersection& intersection) const
{
    if (intersection.marble_is_present()) {
        char letter = intersection.letter();
        int number = intersection.number();

        // letter + number increase
        if (get_intersection(letter, number + 1) and
            get_intersection(letter, number + 2) and
            get_intersection(letter, number + 1)->marble_is_present() and
            get_intersection(letter, number + 2)->state() == VACANT) {
            return true;
        }

        // letter + number decrease
        if (get_intersection(letter, number - 1) and
            get_intersection(letter, number - 2) and
            get_intersection(letter, number - 1)->marble_is_present() and
            get_intersection(letter, number - 2)->state() == VACANT) {
            return true;
        }

        // letter increase + number
        if (get_intersection(letter + 1, number) and
            get_intersection(letter + 2, number) and
            get_intersection(letter + 1, number)->marble_is_present() and
            get_intersection(letter + 2, number)->state() == VACANT) {
            return true;
        }

        // letter decrease + number
        if (get_intersection(letter - 1, number) and
            get_intersection(letter - 2, number) and
            get_intersection(letter - 1, number)->marble_is_present() and
            get_intersection(letter - 2, number)->state() == VACANT) {
            return true;
        }

        // letter increase + number increase
        if (get_intersection(letter + 1, number + 1) and
            get_intersection(letter + 2, number + 2) and
            get_intersection(letter + 1,
                             number + 1)->marble_is_present() and
            get_intersection(letter + 2, number + 2)->state() == VACANT) {
            return true;
        }

        // letter decrease + number decrease
        if (get_intersection(letter - 1, number - 1) and
            get_intersection(letter - 2, number - 2) and
            get_intersection(letter - 1,
                             number - 1)->marble_is_present() and
            get_intersection(letter - 2, number - 2)->state() == VACANT) {
            return true;
        }
    }
}

}} // namespace openxum zertz
